<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Equipment\Equipment::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'category_id' => $faker->randomDigitNotNull ,
        'price' => $faker->numberBetween(200, 1000)
    ];
});
