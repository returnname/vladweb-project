<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Coach\Coach::class, function (Faker $faker) {
    return [
        'firstname' => $faker->firstNameMale,
        'lastname' => $faker->lastName ,
        'birthdate' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'description' => $faker->sentence(6,true),
        'price' => $faker->numberBetween(500,1000),
        'experience' => $faker->randomDigit,
    ];
});
