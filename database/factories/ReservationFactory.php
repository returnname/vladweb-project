<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Reservation\Reservation::class, function (Faker $faker) {
    return [
        'coach_id' => $faker->randomDigitNotNull ,
        'user_id' => $faker->randomDigitNotNull ,
        'time_start'=> $faker->dateTime('now'),
        'time_end'=> $faker->dateTime('now')
    ];
});
