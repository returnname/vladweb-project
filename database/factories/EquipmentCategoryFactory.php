<?php

use Faker\Generator as Faker;


$factory->define(App\Models\Equipment\EquipmentCategory::class, function (Faker $faker) {
    return [
        'name' => $faker->word
    ];
});
