<?php

use Faker\Generator as Faker;

$factory->define(App\Models\MountainBase\MountainBase::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'description' => $faker->sentence(),
        'price' => $faker->numberBetween(0,1200),
        'address' => $faker->address,
        'geo_x' => '56.138094',
        'geo_y' => '40.327208',
        'features' => []
    ];
});
