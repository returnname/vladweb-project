<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Equipment\EquipmentReservation::class, function (Faker $faker) {
    return [
        'equipment_id' => $faker->randomDigitNotNull,
        'reservation_id' => $faker->randomDigitNotNull,
    ];
});
