<?php

use Illuminate\Database\Seeder;

class CoachRatingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('coach_ratings')->insert([
            'value' => 4.5,
            'user_id' => 1,
            'coach_id' => 1
        ]);
    }
}
