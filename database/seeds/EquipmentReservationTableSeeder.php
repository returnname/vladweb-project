<?php

use Illuminate\Database\Seeder;

class EquipmentReservationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Equipment\EquipmentReservation::class, 10)->create();
    }
}
