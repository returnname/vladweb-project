<?php

use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\User\User::class, 1)->create();

        $this->call(CoachTableSeeder::class);
        $this->call(EquipmentTableSeeder::class);
        $this->call(EquipmentCategoryTableSeeder::class);
        $this->call(ReservationTableSeeder::class);
        $this->call(EquipmentReservationTableSeeder::class);
        $this->call(EquipmentTableSeeder::class);
        $this->call(MountainBaseTableSeeder::class);
        $this->call(SitesTableSeeder::class);
        $this->call(UserRolesTableSeeder::class);
        $this->call(CoachRatingsTableSeeder::class);
    }
}
