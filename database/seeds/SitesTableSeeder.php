<?php

use Illuminate\Database\Seeder;

class SitesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sites')->insert([
            'db_name' => 'vladweb',
            'domain' => 'lighthouse.wed',
            'plan_id' => 1,
            'status' => 'active',
            'client_id' => 1,
        ]);
    }
}
