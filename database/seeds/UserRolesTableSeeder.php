<?php

use Illuminate\Database\Seeder;
use App\Models\User\Role;

class UserRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create(
            ['name' => 'Администратор']
        );

        Role::create(
            ['name' => 'Пользователь' ]
        );
    }
}
