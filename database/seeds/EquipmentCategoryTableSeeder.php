<?php

use Illuminate\Database\Seeder;

class EquipmentCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Equipment\EquipmentCategory::class, 10)->create();
    }
}
