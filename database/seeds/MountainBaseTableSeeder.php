<?php

use Illuminate\Database\Seeder;

class MountainBaseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\MountainBase\MountainBase::class, 10)->create();
    }
}
