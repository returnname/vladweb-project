<?php

use Illuminate\Database\Seeder;




class CoachTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Coach\Coach::class, 5)->create();
    }
}
