<?php

Breadcrumbs::register('home', function ($breadcrumbs) {
    $breadcrumbs->push('Home', route('home'));
});


//Инструктора
Breadcrumbs::register('coaches', function ($breadcrumbs) {
    $breadcrumbs->push('Тренера', route('coach.index'));
});
Breadcrumbs::register('coach', function ($breadcrumbs, $coach ) {
    $breadcrumbs->parent('coaches');
    if($coach){
        $breadcrumbs->push($coach->full_name, route('coach.show', $coach->id));
    }
});
//Инструктора
Breadcrumbs::register('mountains', function ($breadcrumbs) {
    $breadcrumbs->push('Горные базы', route('mountain.index'));
});
Breadcrumbs::register('mountain', function ($breadcrumbs, $mountain ) {
    $breadcrumbs->parent('mountains');
    if($mountain){
        $breadcrumbs->push($mountain->name , route('mountain.show', $mountain->id));
    }
});

//Admin panel
Breadcrumbs::register('admin', function ($breadcrumbs) {
    $breadcrumbs->push('Home', route('mountain.index'));
});



