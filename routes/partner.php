<?php

Auth::routes();

Route::get('/', 'HomeController@index');

Route::resources([
    'reserve' => 'ReservationController',
    'coach' => 'CoachController',
    'mountain'=>'MountainBaseController',
]);

Route::resource('review', 'ReviewController', ['only' => ['store']]);

Route::get('/home', 'HomeController@index' )->name('home');
Route::get('/profile', 'ProfileController@index' )->name('profile');

Route::prefix('/payment')->group(function (){
    Route::get('/result', 'PaymentController@result' )->name('payment.result');
    Route::get('/success', 'PaymentController@success' )->name('payment.success');
    Route::get('/fail', 'PaymentController@fail' )->name('payment.fail');
});

Route::prefix('admin')->group(base_path('routes/admin.php'));

Route::group(['middleware' => 'static'], function() {
    Route::get('/{slug}', 'PageController@show');
});
