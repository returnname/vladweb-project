<?php


Route::get('/','MainController@home')->name('home');;

Auth::routes();

Route::resources([
    'reserve' => 'ReservationController',
    'coach' => 'CoachController',
    'mountain'=>'MountainBaseController',
]);

//Route::resource('review', 'ReviewController')->only(['store']);
Route::post('review/{model}/store','ReviewController@store')->name('review.store');

Route::get('find' , 'MainController@finder')->name('finder');
Route::get('/home', 'HomeController@index' )->name('home');
Route::get('/profile', 'ProfileController@index' )->name('profile');

Route::prefix('admin')->group(base_path('routes/admin.php'));

Route::group(['middleware' => 'static'], function() {
    Route::get('/{slug}', 'PageController@show');
});
