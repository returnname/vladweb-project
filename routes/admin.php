<?php

Route::group(['namespace' =>'Admin', 'as' => 'admin.'], function (){

    Route::namespace('Auth')->group(function (){
        Route::get('login' , 'LoginController@showLoginForm')->name('login');
        Route::post('login' , 'LoginController@login');
        Route::post('logout' , 'LoginController@login')->name('logout');
    });

    Route::group(['middleware' =>'admin'], function (){
        Route::get('dashboard', function (){
            return view('admin.dashboard');
        })->name('dashboard');

        $resourceMethods = ['except' => ['show']];
        Route::resource('user', 'UserController', $resourceMethods);
        Route::resource('coach', 'CoachController', $resourceMethods);
        Route::resource('mountain', 'MountainBaseController',  $resourceMethods);
        Route::resource('page', 'StaticPageController',  $resourceMethods);
        Route::resource('review', 'ReviewController',  $resourceMethods);

        Route::group(['as'=>'settings.'],function (){
            Route::get('/settings','SettingsController@edit')->name('edit');
            Route::post('/settings','SettingsController@edit')->name('update');
        });
    });
});

Route::fallback(function (){return redirect()->route('admin.login');});