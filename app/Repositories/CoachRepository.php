<?php

namespace App\Repositories;
use App\Repositories\Source\Repository;

class CoachRepository extends Repository
{

    public function model(){
        return 'App\Models\Coach\Coach';
    }


}