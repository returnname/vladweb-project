<?php
/**
 * Created by PhpStorm.
 * User: А-2017-11
 * Date: 09.01.2018
 * Time: 20:40
 */

namespace App\Repositories\Source;


interface RepositoryInterface
{
    public function all($columns = array('*'));
    public function paginate($perPage = 15, $columns = array('*'));
    public function paginateWith($perPage = 15, $with, $columns = array('*'));
    public function create($data);
    public function update($data, $id);
    public function delete($id);
    public function find($id, $columns = array('*'));
    public function findWith($id, $with, $columns = array('*'));
    public function findBy($field, $value, $columns = array('*'));

}