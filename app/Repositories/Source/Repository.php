<?php
/**
 * Created by PhpStorm.
 * User: А-2017-11
 * Date: 09.01.2018
 * Time: 20:41
 */

namespace App\Repositories\Source;

use App\Services\DomainCheckService;
use Illuminate\Container\Container as App;
use Illuminate\Database\Connection;
use Illuminate\Database\Eloquent\Model;
use Mockery\Exception;

abstract class Repository implements RepositoryInterface
{

    private $app;

    protected $model;

    protected $domain;

    public function __construct(App $app)
    {
        $this->app = $app;
        $this->makeModel();
    }

    abstract function model();

    public function all($columns = array('*')) {
        return $this->model->get($columns);
    }

    public function paginate($perPage = 15, $columns = array('*')) {


        return $this->model->paginate($perPage, $columns);
    }
    public function paginateWith($perPage = 15, $with , $columns = array('*')) {

        return $this->model->with($with)->paginate($perPage, $columns);

    }

    public function create($data) {
        return $this->model->create($data);
    }

    public function update($data, $id) {
        $model = $this->model->find($id);
        $model->update($data);
        return $model;
    }

    public function delete($id) {

        return $this->model->find($id)->delete();
    }

    public function find($id, $columns = array('*')) {

        return $this->model->find($id, $columns);
    }

    public function findWith($id, $with, $columns = array('*')) {

        return $this->model->with($with)->find($id, $columns);
    }

    public function findBy($attribute, $value, $columns = array('*')) {
        return $this->model->where($attribute, '=', $value)->first($columns);
    }

    public function makeModel() {
        $model = $this->app->make($this->model());

        if (!$model instanceof Model)
            throw new Exception("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");

        return $this->model = $model->newQuery();
    }
}