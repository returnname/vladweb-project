<?php
/**
 * Created by PhpStorm.
 * User: А-2017-11
 * Date: 16.01.2018
 * Time: 15:03
 */

namespace App\Repositories;


use App\Repositories\Source\Repository;

class MountainBaseRepository extends Repository
{
    public function model(){
        return 'App\Models\MountainBase\MountainBase';
    }

}