<?php

namespace App\Facades;
use Illuminate\Support\Facades\Facade;

class Robokassa extends Facade
{


    protected static function getFacadeAccessor()
    {
        return 'robokassa';
    }
}