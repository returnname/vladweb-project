<?php

namespace App\Models\Equipment;

use Illuminate\Database\Eloquent\Model;

class EquipmentReservation extends Model
{
    protected $table = 'equipment_reservation';

    protected $fillable = ['equipment_id','reservation_id'];

}
