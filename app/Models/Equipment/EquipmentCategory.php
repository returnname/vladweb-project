<?php

namespace App\Models\Equipment;

use Illuminate\Database\Eloquent\Model;

class EquipmentCategory extends Model
{
    protected $table = 'equipment_category';


    public function equipments(){

        return $this->hasMany('App\Models\Equipment\Equipment','category_id');
    }


}
