<?php

namespace App\Models\Equipment;

use Illuminate\Database\Eloquent\Model;

class Equipment extends Model
{
    protected $table = 'equipments';

    public function category(){

        return $this->belongsTo('App\Models\Equipment\EquipmentCategory');
    }


    public $timestamps = false;
}
