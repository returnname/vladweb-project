<?php

namespace App\Models\User;

use App\Models\Review;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name', 'email', 'password', 'role_id'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role(){
        return $this->belongsTo('App\Models\User\Role','role_id','id');
    }

    public function isAdmin(){
        return $this->role->id == 1;
    }

    public function reservations(){
        return $this->hasMany('App\Models\Reservation\Reservation');
    }

    public function reviews(){
        return $this->hasMany(Review::class);
    }

    public function getImageAttribute($value){
        return asset($value ? "storage/{$value}"  : 'images/no-image.png');
    }
}
