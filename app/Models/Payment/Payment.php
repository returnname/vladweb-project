<?php

namespace App\Models\Payment;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{

    protected $fillable = [
       'reservation_id','status','amount','id','signature'
    ];

    public function scopePayment($query, $inv_id){

        return $query->where('reservation_id', $inv_id)->first();
    }

    public function reservation(){
        return $this->belongsTo('App\Models\Reservation\Reservation');
    }


}
