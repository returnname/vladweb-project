<?php

namespace App\Models\Reservation;

use App\Facades\Robokassa;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $fillable = [
        'coach_id', 'equipment_reservation_id', 'user_id','user_id','time_start','time_end','date'
    ];


    protected $dateFormat = 'j F Y h:i:s';

    public $timestamps = false;

    public function coach(){

        return $this->belongsTo('App\Models\Coach\Coach');
    }

    public function equipments(){

        return $this->hasMany('App\Models\EquipmentReservation\EquipmentReservation');
    }

    public function payment(){


        return $this->hasOne('App\Models\Payment\Payment');
    }

    public function scopeBetweenDates($query, $dates)
    {
        return $query
            ->whereBetween('time_start', $dates)
            ->whereBetween('time_end' , $dates);
    }


    public function getLinkAttribute(){

        if(!$this->payment){

            return false;
        }
        return Robokassa::id($this->id)->sum($this->payment->amount)->description('бронирование')->url();
    }
    public function getStartAttribute(){

        return $this->time_start->format('j F Y h:i:s');
    }

    public function getEndAttribute(){

        return $this->time_end->format('j F Y h:i:s');
    }


    ##TODO DO REFACTOR
    public function setTimeStartAttribute($dt)
    {
        $this->attributes['time_start'] = (new Carbon($dt->format($this->dateFormat)))->minute(0)->second(0);
    }

    public function setTimeEndAttribute($dt)
    {
        $this->attributes['time_end'] = (new Carbon($dt->format($this->dateFormat)))->minute(0)->second(0);
    }
}
