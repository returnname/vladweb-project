<?php

namespace App\Models;


use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $fillable = ['text' , 'rating' , 'user_id' , 'status' , 'reviewable_type'];

    public function reviewable()
    {
        return $this->morphTo();
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

    public function scopeModerating($query)
    {
        return $query->where('status', 'moderating');
    }

    public function scopeActive($query)
    {
        return $query->where('status', 'active');
    }

    public function getDaysAgoAttribute(){

        return $this->created_at->diffForHumans(now());
    }

}