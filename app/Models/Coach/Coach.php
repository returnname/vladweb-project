<?php

namespace App\Models\Coach;

use App\Services\DomainCheckService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;

class Coach extends Model
{

    protected $appends = ['rating' , 'full_name'];

    protected $dates = ['birthdate'];

    protected $fillable = [
        'firstname', 'lastname','experience','description','image','price','birthdate'
    ];

    public function reviews(){

        return $this->morphMany('App\Models\Review', 'reviewable' , 'reviewable_type');
    }

    public function reviewsActive(){

        return $this->reviews()->active();
    }

    public function getRatingAttribute(){

        return $this->reviewsActive->avg('rating');
    }

    public function reservations(){
        return $this->hasMany('App\Models\Reservation\Reservation');
    }

    public function getFullNameAttribute(){
        return "{$this->firstname} {$this->lastname}";
    }

    public function getBirthdayAttribute(){
        return $this->birthdate->format('j F Y');
    }

    public function getBirthdateValueAttribute(){
        return $this->birthdate->toDateString();
    }

    public function getExperienceStringAttribute(){
        $num = $this->attributes['experience'];
        $word = unit_case($num);

        return "{$num} {$word}";
    }

    public function getPriceCurrencyAttribute(){
        $currency = config('client_site.currency');

        return "{$this->price} {$currency}";
    }
    //Кажется не юзается
    public function getImagePath(){
        $path = "app/public/{$this->image}";
        return storage_path($path);
    }

    public function getImageAttribute($value){
        return asset($value ? "storage/{$value}"  : 'images/no-image.png');
    }
}
