<?php

namespace App\Models\MountainBase;

use Illuminate\Database\Eloquent\Model;

class MountainBase extends Model
{
    protected $fillable = [ 'name','description','image','price','address','features' ];

    protected $casts = [ 'features' => 'array' ];

    protected $appends = ['rating'];

    public function getPriceCurrencyAttribute(){
        $currency = config('client_site.currency');

        return "{$this->price} {$currency}";
    }

    public function reviews(){
        return $this->morphMany('App\Models\Review', 'reviewable' , 'reviewable_type');
    }

    public function reviewsActive(){
        return $this->reviews()->active();
    }

    public function getRatingAttribute(){

        return $this->reviewsActive->avg('rating');
    }

    public function getImageAttribute($value){
        return asset($value ? "storage/{$value}"  : 'images/no-image.png');
    }

    public function getFeaturesAttribute($value){
        return $value ? json_decode($value) : [];
    }
}
