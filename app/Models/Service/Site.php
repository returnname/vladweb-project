<?php

namespace App\Models\Service;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{

    public $fillable = ['key', 'domain', 'status', 'client_id', 'plan_id'];


    public $timestamps = false;



    public function isAvailable(){

    }

    public function scopeActive($query){

        return $query->where('status', 'active');
    }



}
