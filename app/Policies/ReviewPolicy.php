<?php

namespace App\Policies;

use App\Models\User\User;
use Illuminate\Auth\Access\HandlesAuthorization;


class ReviewPolicy
{
    use HandlesAuthorization;


    /**
     * @param User $user
     * @param mixed MountainBase || Coach  $model
     * @return mixed
     */
    public function create(User $user, $model){
        return !$model->reviews()->where('user_id' , $user->id)->exists();
    }

}
