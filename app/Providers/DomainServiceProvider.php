<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\DomainCheckService;

class DomainServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('App\Services\DomainCheckService', function ($app) {
            $host = $app->request->getHost();
            $cache = $app['cache'];
            $config = $app['config'];
            return new DomainCheckService($host , $cache, $config);
        });


        $this->app->bind('domain', function ($app) {
            return $app->make('App\Services\DomainCheckService');
        });
    }
}
