<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Robokassa\Robokassa;
use Lexty\Robokassa\Auth;
use Lexty\Robokassa\Payment;

class RobokassaServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('robokassa', function ($app) {

//            $auth = $this->app->makeWith(
//                'Lexty\Robokassa\Payment',
//                [
//                    'merchantLogin'     =>  config('robokassa.login'),
//                    'paymentPassword'   =>  config('robokassa.password'),
//                    'validationPassword'=>  config('robokassa.password_validation'),
//                    'isTest'            =>  config('robokassa.test')
//                ]);

            $auth = new Auth(
                config('robokassa.login'),
                config('robokassa.password'),
                config('robokassa.password_validation'),
                config('robokassa.test')
            );

            $payment = new Payment($auth);


            //$payment = $this->app->makeWith('Lexty\Robokassa\Auth', ['auth' => $auth]);


            return new Robokassa($auth, $payment);
        });
    }


}
