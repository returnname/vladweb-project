<?php

namespace App\Providers;

use App\Models\Page;
use App\Observers\PageObserver;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;


class AppServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bootObservers();
        $this->mapMorphRelations();



        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }


    public function bootObservers(){
        Page::observe(PageObserver::class);
    }

    public function mapMorphRelations(){
        Relation::morphMap([
            'coach' => \App\Models\Coach\Coach::class,
            'mountain' => \App\Models\MountainBase\MountainBase::class,
        ]);;
    }
}
