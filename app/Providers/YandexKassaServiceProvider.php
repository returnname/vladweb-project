<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use YandexCheckout\Client;
use App\YandexKassa\YandexKassa;

class YandexKassaServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(YandexKassa::class, function ($app) {


            return new YandexKassa($this->app->make(config('yandex_kassa.class')));
        });
    }
}
