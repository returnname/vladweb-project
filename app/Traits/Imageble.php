<?php

namespace App\Traits;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

trait Imageble
{

    /**
     * @param $image
     * @param $folder
     * @return string
     */
    public function uploadImage($image,string $folder):string {
        $name =  $image->hashName();
        $path = "{$folder}/{$name}";

        $storage = $this->getStoragePath($path);

        Image::make($image)->resize(500, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save($storage);

        return $path;
    }

    public function getStoragePath($path):string{
        $root = config('filesystems.disks.public.root');
        return "{$root}/{$path}";
    }

    public function deleteImage($path){
        return Storage::disk('public')->delete($path);
    }


}