<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class ClientSiteCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'site:create {db}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($db = $this->argument('db')){
            $this->install($db);
        }

    }

    public function install($db){
        DB::statement("CREATE DATABASE IF NOT EXISTS {$db}");

        Config::set('database.default', 'client');
        Config::set('database.connections.client.database', $db);

        $this->call('migrate:install',['--database' => 'client']);
        $this->call('migrate',[
            '--database' => 'client',
            '--path'    => $this->migrationsPath()
        ]);
    }

    public function migrationsPath(){
        return database_path('migrations/foundation');
    }
}
