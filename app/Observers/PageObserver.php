<?php

namespace App\Observers;

use App\Models\Page;
use Illuminate\Support\Facades\Cache;

class PageObserver
{

    public function created(Page $page)
    {
        Cache::forever("pages.{$page->slug}", $page->slug);
    }

    public function deleted(Page $page)
    {
        Cache::forget("pages.{$page->slug}");
    }
}