<?php

namespace App\Observers;

use App\Models\Payment\Payment;

class PaymentObserver
{


    public function saving(Payment $payment){
        $signature = vsprintf('%s:%01.2f:%u:%s', [
            config('robokassa.login'),
            $payment['amount'],
            $payment['id'],
            config('robokassa.password')
        ]);

        $payment['signature'] = $signature;
    }


}