<?php
/**
 * Created by PhpStorm.
 * User: А-2017-11
 * Date: 16.01.2018
 * Time: 16:47
 */

namespace App\Services;

use App\Traits\Imageble;

class MountainBaseService
{
    public $folder = 'mountains';

    use Imageble;

    public function handleUploadImage($file){

        return $this->uploadImage($file , $this->folder);
    }

    public function handleUpdateImage($file, $oldImage){
        if (!is_null($oldImage)){
            $this->deleteImage($oldImage);
        }

        return $this->uploadImage($file , $this->folder);
    }

}