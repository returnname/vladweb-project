<?php

namespace App\Services;

use App\Traits\Imageble;

class CoachService
{
    use Imageble;

    public $folder = 'avatars';

    public function handleUploadImage($file){

        return $this->uploadImage($file , $this->folder);
    }

    public function handleUpdateImage($file, $oldImage){
        if (!is_null($oldImage)){
            $this->deleteImage($oldImage);
        }

        return $this->uploadImage($file , $this->folder);
    }
}
