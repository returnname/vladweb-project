<?php


namespace App\Services;

use App\Models\Service\Site;
use Illuminate\Cache\CacheManager;
use Illuminate\Config\Repository;
use Illuminate\Support\Facades\Route;

class DomainCheckService
{

    public $host;

    public $status;

    protected $type;

    protected $tenant;

    protected $cache;

    protected $config;

    public function __construct(string $host, CacheManager $cache, Repository $config)
    {
        $this->cache = $cache;
        $this->config = $config;
        $this->host = $host;
        $this->tenant = $this->getSite();
    }


    public function boot(){
        if ($this->isMainDomain()) return $this->type = 'main';

        abort_if(!$this->tenant ,403);

        $this->type = 'partner';
    }


    public function getDomainType(){

        return $this->type ?? false;
    }

    public function isMainDomain(){
        return $this->host === $this->config->get('client_site.main_domain');
    }

    public function isActiveSite(){
       return $this->status === 'active';
    }

    protected function getSite(){
        $this->cache->flush();

        return $this->cache->rememberForever('sites', function () {
            return Site::all();
        })->whereIn('domain', $this->host)->first();
    }

    protected function setDatabaseConnection($site){
        $type = $this->config->get('client_site.' . $this->type . '.db');

        $this->config->set('database.default', $type);

        if ($this->type === 'client' && $site){
            $this->config->set('database.connections.client.database', $site->key);
        }
    }

}