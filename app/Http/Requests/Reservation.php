<?php

namespace App\Http\Requests;



use App\Rules\BeetweenDates;
use Illuminate\Foundation\Http\FormRequest;

class Reservation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'equipment' => 'nullable|array',
            'quantity'  => 'required',
            'dates.*'   => 'required|date',
            'dates'     => ['required','array', new BeetweenDates()],
            'amount'    => 'required|min:1'
        ];
    }
}
