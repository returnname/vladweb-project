<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Coach extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname'     => 'required|max:25',
            'lastname'      => 'required|max:25',
            'experience'    => 'numeric',
            'price'         => 'numeric',
            'description'   => 'required|max:128',
            'image'         => 'file',
            'birthdate'     => 'date',
        ];
    }
}
