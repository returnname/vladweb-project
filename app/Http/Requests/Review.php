<?php
/**
 * Created by PhpStorm.
 * User: А-2017-11
 * Date: 13.02.2018
 * Time: 20:03
 */

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class Review extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'rating' => 'required|numeric',
            'text'   => 'required',
            'reviewable_type'   => 'string',
        ];
    }


}