<?php
if (!function_exists('unit_case')){
    function unit_case($num){
        $titles = config('other.case.years');
        $cases = array(2, 0, 1, 1, 1, 2);

        return $titles[($num % 100 > 4 && $num % 100 < 20) ? 2 : $cases[min($num % 10, 5)]];
    }
}
