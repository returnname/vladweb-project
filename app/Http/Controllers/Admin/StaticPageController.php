<?php

namespace App\Http\Controllers\Admin;

use App\Models\Page;
use App\Http\Requests\Page as PageRequest;
use App\Http\Controllers\Controller;

class StaticPageController extends Controller
{

    public function index(){
        $pages = Page::paginate(15);

        return view('admin.sections.pages.index')->with(compact('pages'));
    }

    public function create(){
        return view('admin.sections.pages.create');
    }

    public function store(PageRequest $request){
        Page::create($request->all());

        return redirect()->route('admin.page.index')->with('message', 'Страница добавлена');
    }

    public function edit(Page $page){
        return view('admin.sections.pages.edit' , compact('page'));
    }

    public function update(PageRequest $request , $id){
        $page = Page::find($id)->first();

        $page->update($request->all());

        return redirect()->route('admin.page.index')->with('message', 'Страница обновлена');
    }

    public function destroy($id){
        Page::destroy($id);

        return redirect()->route('admin.page.index')->with('message', 'Страница удалена');
    }
}