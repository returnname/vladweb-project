<?php

namespace App\Http\Controllers\Admin;

use App\Services\CoachService;
use App\Repositories\CoachRepository;
use App\Http\Requests\Coach as CoachRequest;
use App\Http\Controllers\Controller;

class CoachController extends Controller
{
    protected $coach;

    protected $service;


    public function __construct(CoachRepository $coach , CoachService $service){
        $this->coach = $coach;
        $this->service = $service;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){
        $coaches = $this->coach->paginateWith(15,'reviews');

        return view('admin.sections.coaches.index' , compact('coaches'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id){
        $coach = $this->coach->find($id);
        return view('admin.sections.coaches.show' , compact('coach'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(){
        return view('admin.sections.coaches.create');
    }

    /**
     * @param CoachRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CoachRequest $request){
        $coach = $this->coach->create($request->except('image'));

        if($request->hasFile('image')){
            $coach->image = $this->service->handleUploadImage($request->file('image'));
            $coach->save();
        }

        return redirect()->route('admin.coach.index')->with('message', 'Тренер создан');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id){
        $coach = $this->coach->find($id);

        return view('admin.sections.coaches.edit' , compact('coach'));
    }

    /**
     * @param CoachRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CoachRequest $request, $id){
        $coach = $this->coach->update($request->except('image'), $id);

        if ($request->hasFile('image')){
            $coach->image = $this->service->handleUpdateImage($request->file('image'), $coach->image);
            $coach->save();
        }

        return redirect()->route('admin.coach.index')->with('message', 'Тренер обновлен');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id){
        $this->coach->delete($id);

        return redirect()->route('admin.coach.index')->with('message', 'Тренер удален');
    }

}
