<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\MountainBase as MountainBaseRequest;
use App\Models\MountainBase\MountainBase;
use App\Services\MountainBaseService;
use App\Http\Controllers\Controller;
use App\Repositories\MountainBaseRepository;

class MountainBaseController extends Controller
{

    protected $mountain;
    protected $service;

    public function __construct(MountainBaseRepository $mountain , MountainBaseService $service)
    {
        $this->mountain = $mountain;
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mountains = $this->mountain->paginate(10);

        return view('admin.sections.bases.index', compact('mountains'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.sections.bases.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\MountainBase  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MountainBaseRequest $request)
    {
        $mountain = $this->mountain->create($request->except('image'));

        if($request->hasFile('image')){
            $mountain->image = $this->service->handleUploadImage($request->file('image'));
            $mountain->save();
        }
        return redirect()->route('admin.mountain.index')->with('message', 'Горная база создана');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(MountainBase $mountain)
    {
        return view('admin.sections.bases.show',compact('mountain'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(MountainBase $mountain)
    {
        return view('admin.sections.bases.edit',compact('mountain'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\MountainBase  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MountainBaseRequest $request, MountainBase $mountain)
    {

        $mountain->fill($request->except('image'));

        if (!$request->filled('features'))$mountain->features = null;

        if ($request->hasFile('image')){
            $mountain->image = $this->service->handleUpdateImage($request->file('image'), $mountain->image);
        }

        $mountain->save();

        return redirect()->route('admin.mountain.index')->with('message', 'Горная база обновлена');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->mountain->delete($id);

        return redirect()->route('admin.mountain.index')->with('message', 'Горная база удалена');
    }
}
