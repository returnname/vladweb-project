<?php

namespace App\Http\Controllers\Admin;

use App\Models\Review;
use App\Http\Requests\ReviewModerate;
use App\Http\Controllers\Controller;

class ReviewController extends Controller
{

    public function index(){
        $reviews = Review::with('user')->paginate(10);

        return view('admin.sections.reviews.index', compact('reviews'));
    }

    public function edit($id){
        $review = Review::find($id);

        return view('admin.sections.reviews.edit', compact('review'));
    }

    public function update(ReviewModerate $request , $id){
        $review = Review::find($id)->first();

        $review->update($request->all());

        return redirect()->route('admin.review.index')->with('message', 'Отзыв изменен');
    }
}