<?php

namespace App\Http\Controllers\Admin;


use App\Models\User\Role;
use App\Models\User\User;
use App\Traits\Imageble;
use Illuminate\Http\Request;

class UserController
{
    use Imageble;

    public function index(){
        $users = User::with('role')->paginate(10);
        return view('admin.sections.users.index' , compact('users'));
    }

    public function create(){
        $roles = Role::all();
        return view('admin.sections.users.create', compact('roles'));
    }

    public function store(Request $request){
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $user = User::make([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
            'role_id' => $request['role_id'],
        ]);

        if ($request->has('image')){
            $user->image = $this->uploadImage($request->file('image') , 'users');
        }

        $user->save();

        return redirect()->route('admin.user.index')->with('message' , 'Пользователь добавлен');
    }

    public function edit(User $user){
        $roles = Role::all();
        return view('admin.sections.users.edit', compact('user' , 'roles'));
    }

    public function update(User $user , Request $request){
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,' . $user->id,
            'password' => 'nullable|string|min:6|confirmed',
        ]);

        $user->fill($request->except(['image' , 'password']));

        if ($request->has('image')){
            if ($user->image) $this->deleteImage($user->image);
            $user->image = $this->uploadImage($request->file('image') , 'users');
        }

        if($request->filled('password')){
            $user->password = bcrypt($request->password);
            //тут нужно что-то вроде рефреш токена
        }

        $user->save();

        return redirect()->route('admin.user.index')->with('message' , 'Пользователь обновлен');
    }

    public function destroy(User $user){
        if ($user->image) $this->deleteImage($user->image);
        
        $user->delete();

        return redirect()->route('admin.user.index')->with('message' , 'Пользователь удален');
    }
}