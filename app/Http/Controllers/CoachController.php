<?php

namespace App\Http\Controllers;

use App\Models\Review;
use SEO;
use App\Http\Controllers\Controller;
use App\Repositories\CoachRepository;

class CoachController extends Controller
{
    protected $coach;

    public function __construct(CoachRepository $coach){

        $this->coach = $coach;
    }

    public function index(){
        SEO::setTitle('Тренера');
        $coaches = $this->coach->paginate(4);
        return view('pages.coaches.index', compact('coaches'));
    }

    public function show($id){
        $coach = $this->coach->findWith($id, ['reservations', 'reviews' => function ($query) { $query->active(); }, 'reviews.user']);


        if (auth()->check()){
            $canReview = Review::where('user_id' , auth()->user()->id)->exists();
        }

        SEO::setTitle($coach->full_name);
        SEO::setDescription($coach->description);
        SEO::opengraph()->addImage($coach->image);

        return view('pages.coaches.show', compact('coach' , 'canReview'));
    }
}
