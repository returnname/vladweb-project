<?php


namespace App\Http\Controllers;

use App\Models\Coach\Coach;
use App\Models\MountainBase\MountainBase;
use App\Models\Review;
use App\Http\Requests\Review as ReviewRequest;
use App\Http\Controllers\Controller;


class ReviewController extends Controller
{


    public function store($reviewableModel, ReviewRequest $request){
        #TODO Как-то шляпно и некрасиво
        $review = Review::make($request->all())->user()
            ->associate($request->user());

        $review->reviewable_id = $reviewableModel;
        $review->save();

        return redirect()->back()->with('message', 'Отзыв отправлен. Спасибо!');
    }

}