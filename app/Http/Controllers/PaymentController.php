<?php

namespace App\Http\Controllers;


use App\Facades\Robokassa;
use App\Models\Payment\Payment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentController extends Controller
{

    protected $payment;


    public function __construct(Payment $payment){
        $this->payment = $payment;
    }

    public function result(Request $request){
        if (Robokassa::validate( $request->all(),__FUNCTION__)){
            $this->payment->payment($request['InvId'])->update(['status' => 1]);
            return response(
                Robokassa::success(),200
            );
        }

        return redirect(route('payment.fail'));
    }

    public function success(Request $request){
        if (!Robokassa::validate( $request->all(),__FUNCTION__)){
            return redirect(route('payment.fail'));
        }
        elseif($request->session()->get('order_id') !== $request['InvId']){
            return redirect(route('home'));
        }


        $payment = $this->payment->payment($request['InvId']);

        return view(
            'pages.payments.success' , compact('payment')
        );
    }

    public function fail(Request $request){
        $this->payment->payment($request['InvId'])->update(['status' => 2]);

        return view('pages.payments.fail');
    }
}
