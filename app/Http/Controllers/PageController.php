<?php

namespace App\Http\Controllers;


use App\Models\Page;
use App\Http\Controllers\Controller;


class PageController extends Controller
{

    public function show($slug){
        $page = Page::where('slug', $slug)->first();


        return view('static')->with(compact('page'));
    }


}