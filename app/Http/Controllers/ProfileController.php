<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\Reservation\Reservation;

class ProfileController extends Controller
{


    public function index(){
        $user = Auth::user();

        //$reserves = Reservation::where('user_id', $user->id)->get();

        return view('pages.profile', ['user' => $user ]);
    }
}
