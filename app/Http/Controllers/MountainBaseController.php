<?php

namespace App\Http\Controllers;


use App\Models\Review;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use SEO;
use App\Models\MountainBase\MountainBase;

class MountainBaseController extends Controller
{


    public function index(){
        $hotels = MountainBase::paginate(4);
        return response()->view('pages.mountains.index' , ['hotels' => $hotels]);
    }

    public function show(MountainBase $mountain){
        return view('pages.mountains.show', compact('mountain'));
    }
}
