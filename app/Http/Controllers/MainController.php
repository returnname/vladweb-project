<?php

namespace App\Http\Controllers;

use App\Models\MountainBase\MountainBase;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function home(){
        $hotels = MountainBase::with('reviewsActive')->take(6)->get();

        return response()->view('pages.main' , compact('hotels'));
    }

    public function finder(Request $request){


        dd($request->all());


    }
}
