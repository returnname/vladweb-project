<?php

namespace App\Http\Controllers;

use App\Models\Coach\Coach;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\Models\Reservation\Reservation as Reservation;
use App\Http\Requests\Reservation as ReservationValidatedRequest;

use App\Models\Equipment\EquipmentCategory as EquipmentCategory;

use App\Models\Equipment\EquipmentReservation as EquipmentReservation;

use App\Models\Payment\Payment;
use App\Http\Controllers\Controller;


class ReservationController extends Controller
{

    public function __construct() {
        $this->middleware('auth', ['only' => ['store']]);
    }


    public function index(){
        return view('reservation');
    }

    public function initApi(){
        $categories = EquipmentCategory::with('equipments')->limit(5)->get();
        $equipments = $categories->pluck('equipments')->flatten();
        $coaches = Coach::with('reservations')->get();

        return response()->json(compact('categories', 'equipments', 'coaches'),200);
    }

    
    public function store(ReservationValidatedRequest $request){
        ##TODO ШЛЯПА ПИЗДЕЦ

        $reservation = Reservation::create(
            [
                'coach_id'      => $request->coach_id,
                'user_id'       => Auth::id(),
                'time_start'    => $request->dates[0],
                'time_end'      => $request->dates[1],
                'payment'       => $request->amount
            ]
        );

        foreach ($request['equipment'] as $equipment){
            EquipmentReservation::create([
                'equipment_id'      => $equipment['id'],
                'reservation_id'    => $reservation->id,
                'amount'            => $reservation
            ]);
        }

        Payment::create([
            'reservation_id' => $reservation->id,
            'amount'         => $request->amount
        ]);

        $request->session()->put('order_id', 25);

        return response()->json([
            'success'        => true,
            //'invoice_url'   => Robokassa::id($reservation->id)->sum($reservation->payment)->description('бронирование')->url(),
            'messages'          => ['Дата забронирована , пожалуйста оплатите заказ']
        ],200);
    }
}
