<?php
/**
 * Created by PhpStorm.
 * User: А-2017-11
 * Date: 01.03.2018
 * Time: 13:38
 */

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminRedirectIfAuthenticated
{



    public function handle($request, Closure $next)
    {
        if( auth()->check() && $request->user()->isAdmin() ){
            return redirect()->route('admin.dashboard');
        }

        return $next($request);
    }



}