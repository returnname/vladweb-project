<?php

namespace App\Http\Middleware;

use Closure;

class CheckUserGeoByIP
{

    public function handle($request, Closure $next)
    {
        if (!$request->session()->has('_city')){
            $city = geoip($request->ip())->city;

            $request->session()->put('_city', geoip($request->ip())->city);
        }

        return $next($request);
    }
}
