<?php
namespace App\Robokassa;

use Lexty\Robokassa\Payment;
use Lexty\Robokassa\Auth;

class Robokassa
{

    protected $payment;
    public $auth;

    /**
     * Robokassa constructor.
     * @param Auth $auth
     * @param Payment $payment
     */
    public function __construct(Auth $auth , Payment $payment){
        $this->payment = $payment;
        $this->auth = $auth;
    }


    public function id(int $id){
        $this->payment->setId($id);

        return $this;
    }

    public function sum(float $amount){
        $this->payment->setSum($amount);

        return $this;
    }

    public function description(string $description){
        $this->payment->setDescription($description);

        return $this;
    }

    public function url(){

        return $this->payment->getPaymentUrl();
    }

    public function validate(array $data , string $type){
        if ($type === 'result'){
            return $this->payment->validateResult($data ,false);
        }
        return $this->payment->validateSuccess($data ,false);
    }

    public function success(){
        return $this->payment->getSuccessAnswer();
    }

}