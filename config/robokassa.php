<?php
return [
    'login'     =>  env("ROBOKASSA_LOGIN"),
    'password'   => env('ROBOKASSA_PASSWORD1'),
    'password_validation'=> env('ROBOKASSA_PASSWORD2'),
    'test'            =>  true
];