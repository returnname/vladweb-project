<?php

return [
    'aggregate'=> true,
    'currency' => 'р.',
    'main_domain'=>'vlweb.loc',
    'client'=>[
        'db'    => 'client',
        'namespace'=> 'Client'
    ],
    'service'=>[
        'db'    => 'main',
        'namespace' => 'Service'
    ]
];