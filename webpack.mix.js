let mix = require('laravel-mix');
let tailwindcss = require('tailwindcss');
let axios = require('axios');
let moment = require('moment');
let _ = require('lodash');
let $ = require('jquery');
//let bootstrap = require('bootstrap');

mix.autoload({
    axios: ['axios', 'window.axios'],
    moment: ['moment', 'window.moment'],
    _: ['_', 'window._'],
    jquery: ['$', 'window.jQuery'],
    //bootstrap:['bootstrap','window.bootstrap']
});


//Main public
mix
    .js('resources/assets/main/js/app.js', 'public/main/js/app.js')
    .sass('resources/assets/main/sass/app.scss', 'public/main/css');

//Partner Public
mix
    .react('resources/assets/partner/js/public/app.js', 'public/partner/js')
    .sass('resources/assets/partner/sass/app.scss', 'public/partner/css')
    .options({
        processCssUrls: false,
        postCss: [ tailwindcss('tailwind.js') ],
    });

//Partner Admin
mix
    .sass('resources/assets/partner/sass/admin/app.scss', 'public/partner/admin/css')
    .js('resources/assets/partner/js/admin/app.js', 'public/partner/admin/js/app.js').sourceMaps();