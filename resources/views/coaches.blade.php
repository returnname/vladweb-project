@extends('layouts.app')
@section('breadcrumbs',  Breadcrumbs::render('coaches') )
@section('content')
    <div class="container mx-auto">
        <div class="grid-2-columns">
            @foreach($coaches as $coach)
                <div class="p-4 max-w-md w-full lg:flex">
                    <div class="h-48 lg:h-auto lg:w-48 flex-none bg-cover rounded-t lg:rounded-t-none lg:rounded-l text-center overflow-hidden" style="background-image: url('{{ asset("storage/{$coach->image}")}}')" title="{{$coach->full_name}}">
                    </div>
                    <div class="border-r w-full border-b border-l border-grey-light lg:border-l-0 lg:border-t lg:border-grey-light bg-white rounded-b lg:rounded-b-none lg:rounded-r p-4 flex flex-col justify-between leading-normal">
                        <div class="mb-8">
                            <p class="text-sm text-grey-dark flex items-center">
                                <i class="fas fa-info mr-4"></i>
                                Сноуборд
                            </p>
                            <a class="text-black font-bold text-xl mb-2" href="{{route('coach.show' , $coach->id)}}">
                                {{$coach->full_name}}
                            </a>
                            <p class="text-grey-darker text-base">{{$coach->description}}</p>
                        </div>
                        <div class="flex items-center">
                                <p class="text-grey-darkest">@include('components.rating',['value' => $coach->rating])</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        {{ $coaches->links('vendor.pagination.basic') }}
    </div>
@endsection