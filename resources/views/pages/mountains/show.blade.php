@extends('layouts.main')
@section('breadcrumbs',  Breadcrumbs::render('mountain', $mountain) )
@section('content')
    <div class="container">
        <div class="row">
            <div class="col py-4">
                <h1>
                    {{$mountain->name}}
                    <span class="badge badge-secondary">от {{$mountain->price_currency}}</span>
                </h1>
                <div class="rating">
                    <span class="text-dark">Рейтинг:</span> @include('components.rating',['value' => $mountain->rating ?? 0])
                </div>
            </div>
            <div class="w-100">
                <div class="card">
                    <img class="card-img-top" src="{{$mountain->image}}" alt="{{$mountain->name}}" style="max-height: 180px">
                    <div class="card-body">
                        <div class="w-100">
                            <div class="py-2">
                                <a class="p-2 text-dark" href="https://vk.com/rentmountain">
                                    <i class="fab fa-vk"></i>
                                </a>
                                <a class="p-2 text-dark" href="https://www.youtube.com/rentmountein" >
                                    <i class="fab fa-youtube"></i>
                                </a>
                                <a class="p-2 text-dark" href="https://www.facebook.com/rentmountein">
                                    <i class="fab fa-facebook"></i>
                                </a>
                            </div>
                            <h3 class="py-2">О базе </h3>
                            {{$mountain->description}}
                            <hr>
                            <div class="py-4">
                                <a class="btn btn-dark" href="{{route('mountain.index')}}">Забронировать</a>
                            </div>
                            <div id="map">
                                <h4>На карте</h4>
                                <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3Ad49eff21acc1635d89fdded5ec424924b8a00d1a885a09105897f0c0f43baaaa&amp;source=constructor" width="100%" height="400" frameborder="0"></iframe>
                            </div>
                            <div class="row mt-2">
                                <div class="col-6">
                                    <h4>Отзывы</h4>
                                    <div class="list-group">
                                        @forelse($mountain->reviews as $review)
                                            <a  class="list-group-item list-group-item-action flex-column align-items-start">
                                                <div class="d-flex w-100 justify-content-between">
                                                    <h5 class="mb-1">{{$review->user->name}}</h5>
                                                    <small>{{$review->days_ago}}</small>
                                                </div>
                                                <p class="mb-1">
                                                   {{$review->text}}
                                                </p>
                                                <small>@include('components.rating',['value' => $review->rating])</small>
                                            </a>
                                            @empty
                                            <div class="alert alert-info">
                                                <strong>Отзывов нету</strong>
                                            </div>
                                        @endforelse
                                    </div>
                                </div>
                                <div class="col-6">
                                    <h4>Добавить отзыв</h4>
                                    @if(\Session::has('message'))
                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                            {{ \Session::get('message') }}
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    @endif
                                    @auth
                                            @can('create', [\App\Models\Review::class , $mountain])
                                                <form action="{{route('review.store', ['model' => $mountain ])}}" method="post">
                                                    {{csrf_field()}}
                                                    <input type="hidden" name="reviewable_type" value="mountain">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Отзыв:</label>
                                                        <textarea required class="form-control" name="text" id="" cols="30"></textarea>
                                                    </div>
                                                    <select name="rating" id="example" class="rating-picker">
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                    </select>
                                                    <div class="btn-group mt-2" role="group" aria-label="Basic example">
                                                        <button type="submit" class=" btn btn-dark">Отправить</button>
                                                    </div>
                                                </form>
                                            @else
                                                <div class="alert alert-info">
                                                    Вы уже оставляли отзыв
                                                </div>
                                            @endcan
                                        @else
                                        <div class="alert alert-warning">
                                            <a href="{{route('login')}}">Войдите</a> , чтобы оставить отзыв
                                        </div>
                                    @endauth

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>

    </div>
    </div>
@endsection
