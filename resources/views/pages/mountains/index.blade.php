@extends('layouts.main')
@section('breadcrumbs',  Breadcrumbs::render('mountains') )
@section('content')
        <div class="bazy-block">
            <h1>Популярные базы отдыха</h1>
            <div class="container2">
                <div class="row bz-blocks">
                    @foreach($hotels as $hostel)
                        <div class="col-md-6 my-4">
                            <div class="bz-block m-2">
                                <div class="bz-b1">
                                    <img class="bz-b1-image" src="{{$hostel->image}}">
                                    <div class="features">
                                        <i class="fas fa-wifi"></i>
                                        <i class="fas fa-utensils"></i>
                                        <i class="far fa-heart"></i>
                                        <i class="far fa-clock"></i>
                                    </div>
                                </div>
                                <div class="bz-b2">
                                    <div>
                                        <a class="base-title" href="{{route('mountain.show' , $hostel)}}">{{$hostel->name}}</a>
                                    </div>
                                    <div class="marks">
                                        @include('components.rating',['value' => $hostel->rating ?? 5])
                                        <span>Оценка: {{$hostel->rating ?? 5 }} из 5 (<a href="#">{{ $hostel->reviewsActive->count() }} отзывов</a>)</span>
                                    </div>
                                    <div class="txt">
                                        {{$hostel->description}}
                                    </div>
                                    <div class="options">
                                        <button class="red-btn">от {{$hostel->price}} ₽</button>
                                        <span class="izbrannoe">
                                    <i class="far fa-heart"></i>
                                    В избранное
                                </span>
                                        <a href="{{route('mountain.show' , $hostel) . '#map' }}" class="gps">
                                            <i class="fas fa-map-marker-alt"></i>
                                            На карте
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="p-2">
                        {{ $hotels->links('vendor.pagination.bootstrap-4') }}
                    </div>
                </div>
            </div>
        </div>
@endsection