@extends('layouts.main')

@section('content')

        <!-- Main banner -->
        <div id="main-banner">
            <div class="container1">
                <div class="logo">
                    <img src="images/logo.png">
                    <span>
              <h1>COMPANY LOGO</h1>
              <div>АГРЕГАТОР ВАШЕГО ОТДЫХА</div>
            </span>
                </div>
                <div class="consult-phone">
                    <div><i class="fas fa-phone-volume"></i>   Нужна консультация?</div>
                    <h1>+7 920 625 28-14</h1>
                </div>
                <!-- Search block -->
                <div class="search-block-container">
                    <form action="{{route('finder')}}" class="search-block">
                        <div class="sblock">
                            <i class="fas fa-map-marker-alt"></i>
                            <div class="city-block">
                                <select name="city" class="city">
                                    <option>Владимир</option>
                                    <option>Омск</option>
                                    <option>Орёл</option>
                                </select>
                            </div>
                            <i class="fas fa-angle-down"></i>
                        </div>
                        <div class="sblock">
                            <input type="text" name="date" class="tcal date" placeholder="с 21.03.2018">
                        </div>
                        <div class="sblock">
                            <i class="far fa-moon"></i>
                            <input type="text" name="nigths" class="" placeholder="на 7 ночей">
                        </div>
                        <div class="sblock">
                            <i class="far fa-user"></i>
                            <input type="text" name="adults" class=""  placeholder="2 взрослых">
                        </div>
                        <div class="sblock">
                            <button class="search-btn">
                                <i class="fas fa-search"></i>
                                НАЙТИ БАЗУ
                            </button>
                        </div>
                    </form>
                </div>
                <!-- /Search block -->
            </div>
        </div>
        <!-- /Main banner -->
        <!-- Service block -->
        <div class="service-block">
            <div class="serv-block">
                <div class="serv-block-txt">
                    <img src="images/serv-block-1-img.png">
                    <span>
              Арендовать базу<br>
              отдыха
            </span>
                </div>
            </div>
            <div class="serv-block">
                <div class="serv-block-txt">
                    <img src="images/serv-block-2-img.png">
                    <span>
              Велосипедные<br>
              прогулки
            </span>
                </div>
            </div>
            <div class="serv-block">

                <div class="serv-block-txt">
                    <img src="images/serv-block-3-img.png">
                    <span>
              Сноуборд и<br>
              лыжный спорт
            </span>
                </div>

            </div>
            <div class="serv-block">

                <div class="serv-block-txt">
                    <img src="images/serv-block-4-img.png">
                    <span>
              Активный отдых<br>
              на природе
            </span>
                </div>
            </div>
            <div class="serv-block">
                <div class="serv-block-txt">
                    <img src="images/serv-block-5-img.png">
                    <span>
              Аренда<br>
              экипировки
            </span>
                </div>
            </div>
        </div>
        <!-- /Service block -->

        <!-- Bazy block -->
        <div class="bazy-block">
            <h1>Популярные базы отдыха</h1>
            <div class="container2">

                <div class="bz-blocks row">
                    @foreach($hotels as $hostel)
                        <div class="col-md-6 my-4">
                            <div class="bz-block m-2">
                                <div class="bz-b1">
                                    <img class="bz-b1-image" src="{{$hostel->image}}">
                                    <div class="features">
                                        @foreach($hostel->features as $feature)
                                            <i class="fas fa-{{$feature}}"></i>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="bz-b2">
                                    <div>
                                        <a class="base-title" href="{{route('mountain.show' , $hostel)}}">{{$hostel->name}}</a>
                                    </div>
                                    <div class="marks">
                                        @include('components.rating',['value' => $hostel->rating ?? 5])
                                        <span>Оценка: {{$hostel->rating ?? 0 }} из 5 (<a href="#">{{ $hostel->reviewsActive->count() }} отзывов</a>)</span>
                                    </div>
                                    <div class="txt">
                                        {{$hostel->description}}
                                    </div>
                                    <div class="options">
                                        <button class="red-btn">от {{$hostel->price}} ₽</button>
                                        <span class="izbrannoe">
                                            <i class="far fa-heart"></i>
                                            В избранное
                                        </span>
                                        <a href="{{route('mountain.show' , $hostel) . '#map' }}" class="gps">
                                            <i class="fas fa-map-marker-alt"></i>
                                            На карте
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <a href="{{route('mountain.index')}}" class="more-btn">БОЛЬШЕ ПРЕДЛОЖЕНИЙ</a>
            </div>
        </div>
        <!-- /Bazy block -->


        <!-- Instruktory block -->
        <div class="instruktory-block">
            <img src="images/bike.png" class="bike">
            <div class="row container2">
                <div class="offset-xl-6 offset-lg-6 offset-md-2 col-xl-5 col-lg-4 col-md-10">
                    <div class="txt">
                        <span>
                            <img src="images/checked-img.png"> <span>Хотите научиться кататься на маунтин-байке или сноуборде?</span>
                        </span>
                        <span>
                            <img src="images/checked-img.png"><span>Хотите порхать как бабочка и жалить как пчела?</span>
                        </span>
                        <span>
                            <img src="images/checked-img.png"><span>Хотите обтекать и быть водой?</span>
                        </span>
                        <button class="btn1">
                            <i class="fas fa-search"></i>  подберите инструктора для себя
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Instruktory block -->


        <!-- Instruktory -->
        <div class="instruktory">
            <div class="row justify-content-center container2">
                <h1>Наши инструкторы</h1>
                <div class="col-lg-3 col-xl-2 col-md-4 col-sm-5">
                    <div class="desc-block">
                        <img src="images/instr1.png">
                        <div class="desc">
                            <h1>Гришин</h1>
                            <h2>Владислав</h2>
                            <div>Тренер по фрирайду</div>
                            <span>Опыт 12 лет</span>
                        </div>

                    </div>

                </div>
                <div class="col-lg-3 col-xl-2 col-md-4 col-sm-5">
                    <div class="desc-block">
                        <img src="images/instr2.png">
                        <div class="desc">
                            <h1>Понамарев</h1>
                            <h2>Егор</h2>
                            <div>ТИнструктор-саентолог</div>
                            <span>Опыт 10 лет</span>
                        </div>

                    </div>

                </div>
                <div class="col-lg-3 col-xl-2 col-md-4 col-sm-5">
                    <div class="desc-block">
                        <img src="images/instr3.png">
                        <div class="desc">
                            <h1>Титов</h1>
                            <h2>Тимур</h2>
                            <div>Гуру маунтин-байка</div>
                            <span>Опыт 23 года</span>
                        </div>

                    </div>
                </div>
                <div class="col-lg-3 col-xl-2 col-md-4 col-sm-5">
                    <div class="desc-block">
                        <img src="images/instr4.png">
                        <div class="desc">
                            <h1>Королёва</h1>
                            <h2>Надежда</h2>
                            <div>Тренер по йоге и пилатесу</div>
                            <span>Опыт 3 года</span>
                        </div>

                    </div>
                </div>
                <div class="col-lg-3 col-xl-2 col-md-4 col-sm-5">
                    <div class="desc-block">
                        <img src="images/instr5.png">
                        <div class="desc">
                            <h1>Беломестнов</h1>
                            <h2>Кирилл</h2>
                            <div>Тренер по вэйкборду</div>
                            <span>Опыт 3 года</span>
                        </div>

                    </div>
                </div>
            </div>
            <a href="{{route('coach.index')}}" class="more-btn">СМОТРЕТЬ ЕЩЁ</a>
        </div>
        <!-- /Instruktory -->


        <!-- Arenda snaryazhenia -->
        <div class="arenda-block">
            <img src="images/girl.png" alt="girl" class="girl">
            <div class="container2">
                <h1 class="arenda-h">АРЕНДА СНАРЯЖЕНИЯ</h1>
                <ul class="arenda-sp">
                    <li>
                        <div><img src="images/arenda-img-1.png"></div>
                        <span>Для катания <br>на лыжах</span>
                    </li>
                    <li>
                        <div><img src="images/arenda-img-2.png"></div>
                        <span>Для катания<br>на сноуборде</span>
                    </li>
                    <li>
                        <div><img src="images/arenda-img-3.png"></div>
                        <span>Для походов<br>и активного отдыха</span>
                    </li>
                </ul>
                <div class="txt">
                    <p>Язык образов решительно преобразует даосизм. Ощущение мира амбивалентно трансформирует закон внешнего мира. Моцзы, Сюнъцзы и другие считали, что закон внешнего мира индуцирует гедонизм, открывая новые горизонты. Априори, конфликт прост.</p>
                    <p>
                        Локаята трансформирует непредвиденный бабувизм. Аналогия дискредитирует дедуктивный метод. Единственной космической субстанцией Гумбольдт считал материю, наделенную внутренней активностью, несмотря на это сомнение создает напряженный конфликт.
                    </p>
                </div>
                <button class="btn1"><i class="fas fa-search"></i> подберите снаряжение</button>
            </div>
        </div>
        <!-- /Arenda snaryazhenia -->

        <!-- Map -->
        <div class="map-block">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15406.620512738464!2d73.27345116850145!3d54.96945422916924!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x43ab017076edcbf7%3A0x2bb3dd200acffa5f!2z0JzQtdCz0LAg0J7QvNGB0Lo!5e0!3m2!1sru!2s!4v1521989624694" width="" height="" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
        <!-- /Map -->


@endsection