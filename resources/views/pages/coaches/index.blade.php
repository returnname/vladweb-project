@extends('layouts.main')
@section('breadcrumbs',  Breadcrumbs::render('mountains') )
@section('content')
        <div class="bazy-block">
            <h1>Наши тренеры</h1>
            <div class="container2">
                <div class="row bz-blocks">
                    @foreach($coaches as $coach)
                        <div class="col-md-6 my-4">
                            <div class="bz-block">
                                <div class="bz-b1">
                                    <img  class="bz-b1-image" src="{{$coach->image}}">
                                </div>
                                <div class="bz-b2">
                                    <div>
                                        <a class="base-title" href="{{route('coach.show' , $coach)}}">{{$coach->full_name}}</a>
                                    </div>
                                    <div class="marks">
                                        @include('components.rating',['value' => $coach->rating ?? 5])
                                        <span>Оценка: {{$coach->rating ?? 5 }} из 5 (<a href="#">{{ $coach->reviewsActive->count() }} отзывов</a>)</span>
                                    </div>
                                    <div class="txt">
                                        {{$coach->description}}
                                    </div>
                                    <div class="options">
                                        <button class="red-btn">от {{$coach->price}} ₽</button>
                                        <span class="izbrannoe"><i class="far fa-heart"></i>В избранное</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="w-100 p-2">
                        {{ $coaches->links('vendor.pagination.bootstrap-4') }}
                    </div>
                </div>
            </div>
        </div>
@endsection