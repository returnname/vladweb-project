@extends('layouts.app')

@section('breadcrumbs', Breadcrumbs::render('coach', $coach))

@section('content')
    <div class="container mx-auto">
        <h1>{{$coach->full_name}}</h1>
        <div> @include('components.rating',['value' => $coach->rating])</div>
        <div class="card shadow mx-2">
            <div class="flex mb-4 mt-6">
                <div class="cover-image" style="background-image: url('{{ asset("storage/{$coach->image}")}}')">
                </div>
                <div class="w-2/3 bg-grey-lightest p-8" >

                    <div class="py-8">
                        <a href="https://vk.com/rentmountain" class="ml-4 min-w-full text-center bg-blue-light rounded-full w-24 h-24 p-2 hover:bg-blue">
                            <i class="fab fa-vk"></i>
                        </a>
                        <a href="https://www.youtube.com/rentmountein" class="ml-4 min-w-full text-center bg-red-light rounded-full w-24 h-24 p-2 hover:bg-red">
                            <i class="fab fa-youtube"></i>
                        </a>
                        <a href="https://www.facebook.com/rentmountein" class="ml-4 min-w-full text-center bg-blue-dark rounded-full w-24 h-24 py-2 px-4 hover:bg-red">
                            <i class="fab fa-facebook"></i>
                        </a>
                    </div>

                    <h3 class="mb-4">О инструкторе</h3>
                    {{$coach->description}}
                    <h3 class="my-4">Цена</h3>
                    {{$coach->price_currency}} в час
                    <hr>
                    <div class="mt-6">
                        <a class="btn btn-blue " href="{{route('reserve.index')}}">Забронировать</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="block">
            <div class="card shadow w-1/2 float-left">
                <div class="w-full p-4">
                    <h3 class="">Отзывы</h3>
                </div>
                <div class="flex flex-col">
                @forelse($coach->reviews as $review)
                        <div class="w-full bg-white border-t border-b">
                                <div class="bg-grey-lightest p-4 pl-8 flex border-t border-grey-lighter">
                                    <div class="w-12 mr-2 flex-no-shrink">
                                        <img class="p-2 rounded rounded-full" src="https://scontent-frt3-1.cdninstagram.com/t51.2885-19/s150x150/22638783_1031626323645160_6412994168498421760_n.jpg">
                                    </div>
                                    <div class="p-2 flex-grow">
                                        <header>
                                            <a href="#" class="text-black no-underline">
                                                <span class="font-medium">{{$review->user->name}}</span>
                                            </a>
                                            <div class="text-xs text-grey flex items-center my-1">
                                                <i class="far fa-calendar-alt"></i>
                                                <span class="ml-2">{{$review->created_at}}</span>
                                            </div>
                                        </header>
                                        <article class="py-4 text-grey-darkest leading-normal">
                                            {{$review->text}}
                                        </article>
                                        <footer class="border-t border-grey-lighter text-sm flex">

                                        </footer>
                                    </div>
                                </div>
                            </div>
                @empty
                    <div class="p-4">
                        Отзывов еще нет.
                    </div>
                @endforelse
                </div>
            </div>
            <div class="card w-1/2 float-left p-4">
                @auth
                <div class="w-full p-8">
                    <h3 class="">Написать отзыв</h3>
                </div>
                    @if($msg = session('message'))
                    <div class="w-full p-8 bg-green">
                        <h3 class="">{{ $msg }}</h3>
                    </div>
                    @endif
                    @if($canReview)
                        <form action="{{route('review.store')}}" method="post" class="p-4">
                            {{csrf_field()}}
                            <input type="hidden" name="review_type" value="coach">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <input type="hidden" value="{{$coach->id}}" name="id">
                            <div class="form-field">
                                <div class="w-full">
                                    <label class="form-field__label" for="inline-full-name">
                                        Отзыв:
                                    </label>
                                    <textarea name="text" rows="10" class="form-field__text"></textarea>
                                </div>
                            </div>
                            <div class="form-field">
                                <div class="md:w-1/3">
                                    <label class="form-field__label" for="inline-full-name">
                                        Оценка
                                    </label>
                                </div>
                                <div class="md:w-2/3">
                                    <div id="select-rating"></div>
                                </div>
                            </div>
                            <div class="form-field">
                                <button class="btn btn-blue" type="submit">Отправить</button>
                            </div>
                        </form>
                    @else
                        <div class="p-4">
                            Вы уже оставляли отзыв
                        </div>
                    @endif
                @else
                    <div class="p-6">
                        Зарегистрийтесь или войдите чтобы оставить отзыв
                    </div>
                @endauth
            </div>
        </div>


    </div>
@endsection