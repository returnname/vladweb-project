@extends('layouts.main')

@section('content')
    <div class="container my-2">
        <div class="row justify-content-center d-flex">
            <div class=" col-6">
                <div class="card d-flex ">
                    <div class="card-header">
                        Войти
                    </div>
                    <div class="card-body">

                            @if($errors->any())
                                <div class="alert alert-danger" role="alert">
                                    @foreach($errors->all() as $error)
                                        <span>{{$error}}</span>
                                    @endforeach
                                </div>
                            @endif
                            <form method="POST" action="{{ route('login') }}">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email:</label>
                                    <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Пароль:</label>
                                    <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                                </div>
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                    <label class="form-check-label" for="exampleCheck1">Запомнить меня</label>
                                </div>
                                <div class="btn-group mt-4" role="group" aria-label="Basic example">
                                    <button type="submit" class=" btn btn-outline">Войти</button>
                                    <a href="{{route('register')}}" class="btn btn-info">Регистрация</a>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>

        </div>
    </div>
@endsection