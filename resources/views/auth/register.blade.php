
@extends('layouts.main')

@section('content')
    <div class="container my-2">
        <div class="row justify-content-center d-flex">
            <div class=" col-6">
                <div class="card d-flex ">
                    <div class="card-header">
                        Регистрация
                    </div>
                    <div class="card-body">
                        @if($errors->any())
                            <div class="alert alert-danger" role="alert">
                                @foreach($errors->all() as $error)
                                    <div>{{$error}}</div>
                                @endforeach
                            </div>
                        @endif
                        <form method="POST" action="{{ route('register') }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email:</label>
                                <input type="email" name="email" class="form-control" placeholder="Введите Email">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Логин:</label>
                                <input type="text" name="name" class="form-control" placeholder="Введите логин">
                            </div>
                            <div class="form-group">
                                <label>Пароль:</label>
                                <input type="password" name="password" class="form-control"  placeholder="Пароль">
                            </div>
                            <div class="form-group">
                                <label>Пароль:</label>
                                <input type="password" name="password_confirmation" class="form-control"  placeholder="Подтверждение пароля">
                            </div>
                            <button type="submit" class="mt-4 btn btn-outline">Регистрация</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
