<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    {!! SEO::generate() !!}

    <!-- Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body class="app header-fixed sidebar-fixed aside-menu-off-canvas aside-menu-hidden breadcrumb-fixed">
<header class="app-header navbar">
    <div class="container-fluid">
        <button class="navbar-toggler mobile-sidebar-toggler d-lg-none mr-auto" type="button">
            <span class="navbar-toggler-icon"></span>
        </button>

        <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button">
            <span class="navbar-toggler-icon"></span>
        </button>

        <ul class="nav navbar-nav d-md-down-none">
            {{--<li class="nav-item px-3">--}}
                {{--<a class="nav-link" href="{{route('admin.settings.edit')}}">Настройки</a>--}}
            {{--</li>--}}
        </ul>
        <ul class="nav navbar-nav ml-auto">
            <li class="nav-item d-md-down-none">
                {{--<a class="nav-link" href="#"><i class="icon-bell"></i><span class="badge badge-pill badge-danger">5</span></a>--}}
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <img src="{{auth()->user()->image}}" class="img-avatar" alt="{{auth()->user()->name}}" style="max-width:35px">
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <div class="dropdown-header text-center">
                        <strong>Account</strong>
                    </div>
                    {{--<a class="dropdown-item" href="#"><i class="fa fa-tasks"></i>Профиль<span class="badge badge-danger">42</span></a>--}}
                    <form action="{{route('admin.logout')}}" method="post">
                        {{csrf_field()}}
                        <button type="submit" class="dropdown-item" href=""><i class="fa fa-arrow-left"></i>
                            Выйти
                        </button>
                    </form>
                </div>
            </li>
        </ul>
    </div>

    {{--<button class="navbar-toggler aside-menu-toggler" type="button">--}}
        {{--<span class="navbar-toggler-icon"></span>--}}
    {{--</button>--}}

</header>

<div id="app" class="app-body">
    <!-- Sidebar -->
    @include('admin.components.left_menu')
    <!-- Main content -->
    <main class="main">

        <!-- Breadcrumb -->
        @yield('breadcrumbs')

        <div class="container-fluid">

            <div class="animated fadeIn">
                @yield('content')
            </div>

        </div>
        <!-- /.conainer-fluid -->
    </main>
</div>
<footer class="app-footer">
    <span><a>ВладВеб</a> © 2018 </span>
</footer>
<!-- CSS -->
<link rel="stylesheet" href="{{asset('admin/css/app.css')}}">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="{{asset('admin')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css">
<!-- JS -->
<script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('admin/js/coreui.js')}}"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<script src="{{asset('admin/js/custom.js')}}"></script>
</body>
</html>
