<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {!! SEO::generate() !!}

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">
</head>
    <body>
        @include('components.header')
        @yield('content')
        @include('components.footer')
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}" >
        <link rel="stylesheet" href="{{asset('css/style.css')}}" >
        <link rel="stylesheet" href="{{asset('css/tcal.css')}}" >
        <script src="{{asset('js/jquery.min.js')}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="{{asset('js/bootstrap.min.js')}}"></script>
        <script src="{{asset('js/tcal.js')}}"></script>
        <script src="{{asset('vendor/bootstrap-select/js/bootstrap-select.min.js')}}"></script>
        <link rel="stylesheet" href="{{asset('vendor/bootstrap-select/css/bootstrap-select.min.css')}}">
        <script type="application/javascript" src="{{asset('vendor/jquery-bar-rating/jquery.barrating.min.js')}}"></script>
        <link rel="stylesheet" href="{{asset('vendor/jquery-bar-rating/themes/css-stars.css')}}">
        <script src="{{asset('js/custom.js')}}"></script>
    </body>
</html>
