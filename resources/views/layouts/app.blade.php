<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    {!! SEO::generate() !!}

    <!-- Styles -->
    <link rel="stylesheet" href="{{asset('partner/css/app.css')}}" >
</head>
<body class="uk-background-muted">
    <header class="page-head">
        @include('components.header')
    </header>
    @yield('breadcrumbs')
    <div id="app">
        @yield('content')
    </div>
    @include('components.footer')
    <script type="text/javascript" src="{{asset('partner/js/app.js')}}"></script>
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
</body>
</html>
