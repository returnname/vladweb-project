@extends('layouts.app')
@section('content')
    <div class="container mx-auto">
        <h1 class="mt-8">{{$page->title}}</h1>
        <div class="w-full bg-white shadow p-4 mt-4">
            {!!  $page->content !!}
        </div>
    </div>
@endsection





