@extends('layouts.admin')


@section('content')
    <div class="row">
        <div class="col-md-9">
          <div class="card card-accent-secondary">
              <div class="card-header">
                  <i class="fa fa-align-justify"></i>
                  Все тренера
              </div>
              <div class="card-body">
                  @if($msg = session('message'))
                      <div class="alert alert-success" role="alert">
                          {{ $msg }}
                      </div>
                  @endif
                  <table class="table table-striped">
                      <thead>
                      <tr>
                          <th>Имя</th>
                          <th>Фамилия</th>
                          <th>Рейтинг</th>
                          <th>Статус</th>
                          <th></th>
                      </tr>
                      </thead>
                      <tbody>
                      @foreach($coaches as $coach)
                          <tr>
                              <td>{{$coach->firstname}}</td>
                              <td>{{$coach->lastname}}</td>
                              <td>@include('components.rating',['value' => $coach->rating])</td>
                              <td><span class="badge badge-secondary">active</span></td>
                              <td><a href="{{route('admin.coach.edit', $coach->id)}}" class="btn btn-primary">Редактировать</a></td>
                          </tr>
                      @endforeach
                      </tbody>
                  </table>
                  {{ $coaches->links('vendor.pagination.bootstrap-4') }}
              </div>
          </div>
        </div>
        <div class="col-md-3">
            <div class="card">
                <div class="card-body">
                    <a href="{{route('admin.coach.create')}}" class="btn btn-secondary">Добавить</a>
                </div>
            </div>
        </div>
    </div>
@endsection