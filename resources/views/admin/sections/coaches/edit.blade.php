@extends('layouts.admin')


@section('content')
    <div class="row">
        <div class="col-md-9">
            <form action="{{route('admin.coach.update',$coach->id)}}" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
                {{method_field('PUT')}}
                <div class="card card-accent-secondary">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i>
                        Изменить тренера
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="text-input">Имя</label>
                                    <div class="col-md-9">
                                        <input type="text" id="text-input" name="firstname" class="form-control" placeholder="Имя" value="{{$coach->firstname}}">
                                    </div>
                                </div>
                        <div class="form-group row">
                            <label class="col-md-3 form-control-label" for="text-input">Фамилия</label>
                            <div class="col-md-9">
                                <input type="text" id="text-input" name="lastname" class="form-control" placeholder="Фамилия" value="{{$coach->lastname}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 form-control-label" for="text-input">Кол-во лет опыта</label>
                            <div class="col-md-9">
                                <input type="number" step="0.5" min="0"  id="text-input" name="experience" class="form-control" placeholder="Кол-во лет опыта" value="{{$coach->experience}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 form-control-label" for="text-input">Описание</label>
                            <div class="col-md-9">
                                <input type="textarea-input" rows="4" id="textarea-input" name="description" class="form-control" placeholder="Описание" value="{{$coach->description}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 form-control-label" for="text-input">Стоимость часового занятия</label>
                            <div class="col-md-9">
                                <input type="number" id="text-input" name="price" class="form-control" placeholder="Стоимость часового занятия" value="{{$coach->price}}">
                            </div>
                        </div>
                            <div class="form-group row">
                                <label class="col-md-3 form-control-label" for="text-input">Дата рождения</label>
                                <div class="col-md-9">
                                    <input type="date" id="date-input" name="birthdate" class="form-control" placeholder="Дата рождения" value="{{$coach->birthdate_value}}">
                                </div>
                            </div>
                        <div class="form-group row">
                            <label class="col-md-3 form-control-label" for="image-input">Фотография</label>
                            <div class="col-md-9">
                                <div>
                                    <img src="{{$coach->image}}" class="img-responsive img-thumbnail" style="max-width: 200px">
                                </div>
                                <input type="file" id="image-input" name="image" class="form-control" placeholder="Фотография">
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i>Сохранить</button>
                        <button type="reset" class="btn btn-sm btn-danger"><i class="fa fa-ban"></i>Сбросить</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-3">
            <div class="card">
                <div class="card-body">
                </div>
            </div>
        </div>
    </div>
@endsection