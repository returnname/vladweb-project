@extends('layouts.admin')


@section('content')
    <div class="row">
        <div class="col-md-9">
            <form action="{{route('admin.settings.update')}}" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
                {{method_field('PUT')}}
                <div class="card card-accent-secondary">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i>
                        Настройки сайта
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                            <div class="form-group row">
                                <label class="col-md-3 form-control-label" for="text-input">Имя сайта</label>
                                <div class="col-md-9">
                                    <input type="text" id="text-input" name="company" class="form-control" placeholder="Имя сайта" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 form-control-label" for="text-input">Логотип</label>
                                <div class="col-md-9">
                                    <input type="file" id="text-input" name="company" class="form-control" placeholder="Имя сайта" value="">
                                </div>
                            </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i>Сохранить</button>
                        <button type="reset" class="btn btn-sm btn-danger"><i class="fa fa-ban"></i>Сбросить</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-3">
            <div class="card">
                <div class="card-body">
                </div>
            </div>
        </div>
    </div>


@endsection