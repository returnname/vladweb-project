@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-9">
            <form action="{{route('admin.user.store')}}" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="card card-accent-secondary">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i>
                        Добавить пользователя
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                            <div class="form-group row">
                                <label class="col-md-3 form-control-label" for="text-input">Имя:</label>
                                <div class="col-md-9">
                                    <input type="text" id="text-input" name="name" class="form-control" value="{{old('name')}}" placeholder="Логин">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 form-control-label" for="text-input">E-mail</label>
                                <div class="col-md-9">
                                    <input type="text" id="text-input" name="email" class="form-control" value="{{old('email')}}" placeholder="E-mail">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 form-control-label" for="text-input">Пароль</label>
                                <div class="col-md-9">
                                    <input type="password"  name="password" class="form-control" placeholder="Пароль">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 form-control-label" for="text-input">Пароль</label>
                                <div class="col-md-9">
                                    <input type="password" name="password_confirmation" class="form-control"  placeholder="Подтверждение пароля">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 form-control-label" for="text-input">Услуги</label>
                                <div class="col-md-9">
                                    <select name="role_id" class="form-control selectpicker" style="width: 100%" >
                                        @foreach($roles as $role)
                                            <option value="{{$role->id}}">{{$role->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 form-control-label" for="image-input">Фотография</label>
                                <div class="col-md-9">
                                    <input type="file" id="image-input" name="image" class="form-control" placeholder="Фотография">
                                </div>
                            </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i>Сохранить</button>
                        <button type="reset" class="btn btn-sm btn-danger"><i class="fa fa-ban"></i>Сбросить</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-3">
            <div class="card">
                <div class="card-body">
                </div>
            </div>
        </div>
    </div>
@endsection