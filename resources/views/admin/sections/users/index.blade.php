@extends('layouts.admin')
@section('content')
    <div class="row">
        <div class="col-md-9">
            <div class="card card-accent-secondary">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i>
                    Горы
                </div>
                <div class="card-body">
                    @if($msg = session('message'))
                        <div class="alert alert-success" role="alert">
                            {{$msg}}
                        </div>
                    @endif
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Логин</th>
                            <th>Email</th>
                            <th>Роль</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->role->name}}</td>
                                <td>
                                    <a href="{{route('admin.user.edit', $user->id)}}" class="btn btn-primary">
                                        Редактировать
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $users->links('vendor.pagination.bootstrap-4') }}
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card">
                <div class="card-body">
                    <a href="{{route('admin.user.create')}}" class="btn btn-secondary">Добавить</a>
                </div>
            </div>
        </div>
    </div>
@endsection