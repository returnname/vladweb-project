@extends('layouts.admin')
@section('content')
    <div class="row">
        <div class="col-md-9">
            <div class="card card-accent-secondary">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i>
                    Горы
                </div>
                <div class="card-body">
                    @if($msg = session('message'))
                        <div class="alert alert-success" role="alert">
                            {{$msg}}
                        </div>
                    @endif
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Имя</th>
                            <th>Цена</th>
                            <th>Адрес</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($mountains as $mountain)
                            <tr>
                                <td>{{$mountain->name}}</td>
                                <td><span class="badge badge-primary">{{$mountain->price_currency }}</span></td>
                                <td>{{$mountain->address }}</td>
                                <td>
                                    <a href="{{route('admin.mountain.edit', $mountain->id)}}" class="btn btn-primary">
                                        Редактировать
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $mountains->links('vendor.pagination.bootstrap-4') }}
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card">
                <div class="card-body">
                    <a href="{{route('admin.mountain.create')}}" class="btn btn-secondary">Добавить</a>
                </div>
            </div>
        </div>
    </div>
@endsection