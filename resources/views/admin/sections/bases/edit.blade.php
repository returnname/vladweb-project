@extends('layouts.admin')


@section('content')
    <div class="row">
        <div class="col-md-9">
            <form action="{{route('admin.mountain.update',$mountain->id)}}" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
                {{method_field('PUT')}}
                <div class="card card-accent-secondary">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i>
                        Изменить горную базу
                    </div>
                    <div class="card-body">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="form-group row">
                                <label class="col-md-3 form-control-label" for="text-input">Название</label>
                                <div class="col-md-9">
                                    <input type="text" id="text-input" name="name" class="form-control" placeholder="Фамилия" value="{{$mountain->name}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 form-control-label" for="text-input">Цена в час</label>
                                <div class="col-md-9">
                                    <input type="text" id="text-input" name="price" class="form-control" placeholder="Кол-во лет опыта" value="{{$mountain->price}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 form-control-label" for="text-input">Описание</label>
                                <div class="col-md-9">
                                    <input type="textarea-input" rows="4" id="textarea-input" name="description" class="form-control" placeholder="Описание" value="{{$mountain->description}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 form-control-label" for="text-input">Адрес</label>
                                <div class="col-md-9">
                                    <input type="text" id="text-input" name="address" class="form-control" placeholder="Стоимость часового занятия" value="{{$mountain->address}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 form-control-label" for="text-input">Услуги</label>
                                <div class="col-md-9">
                                    <select name="features[]" class="form-control selectpicker" style="width: 100%" multiple>
                                        <option {{in_array('wifi' , $mountain->features) ? 'selected' : '' }} value="wifi">Wi-fi</option>
                                        <option {{in_array('utensils' , $mountain->features) ? 'selected' : ''}} value="utensils">Кухня</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                            <label class="col-md-3 form-control-label" for="image-input">Фотография</label>
                            <div class="col-md-9">
                                <div>
                                    <img src="{{$mountain->image}}" class="img-responsive img-thumbnail" style="max-width: 200px">
                                </div>
                                <input type="file" id="image-input" name="image" class="form-control" placeholder="Фотография">
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i>Сохранить</button>
                        <button type="reset" class="btn btn-sm btn-danger"><i class="fa fa-ban"></i>Сбросить</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-3">
            <div class="card">
                <div class="card-body">
                </div>
            </div>
        </div>
    </div>
@endsection