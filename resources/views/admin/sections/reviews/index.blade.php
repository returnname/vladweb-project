@extends('layouts.admin')


@section('content')
    <div class="row">
        <div class="col-md-9">
          <div class="card card-accent-secondary">
              <div class="card-header">
                  <i class="fa fa-align-justify"></i>
                  Отзывы
              </div>
              <div class="card-body">
                  @if($msg = session('message'))
                      <div class="alert alert-success" role="alert">
                          {{ $msg }}
                      </div>
                  @endif
                  <table class="table table-striped">
                      <thead>
                      <tr>
                          <th>Имя</th>
                          <th>Дата</th>
                          <th>Статус</th>
                          <th>Оценка</th>
                          <th></th>
                      </tr>
                      </thead>
                      <tbody>
                      @foreach($reviews as $review)
                          <tr>
                              <td>{{$review->user->name}}</td>
                              <td>{{$review->created_at}}</td>
                              <td>
                                  @include('admin.components.status',[ 'status' => $review->status ])
                              </td>
                              <td>@include('components.rating',['value' => $review->rating])</td>
                              <td><a href="{{route('admin.review.edit', $review->id)}}" class="btn btn-primary">Открыть</a></td>
                          </tr>
                      @endforeach
                      </tbody>
                  </table>
                  {{ $reviews->links('vendor.pagination.bootstrap-4') }}
              </div>
          </div>
        </div>
        <div class="col-md-3">
            <div class="card">
                <div class="card-body">
                    <a href="{{route('admin.coach.create')}}" class="btn btn-secondary">Добавить</a>
                </div>
            </div>
        </div>
    </div>
@endsection