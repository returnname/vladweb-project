@extends('layouts.admin')


@section('content')
    <div class="row">
        <div class="col-md-9">
            <div class="card card-accent-secondary">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i>
                    Статические страницы
                </div>
                <div class="card-body">
                    @if($msg = session('message'))
                        <div class="alert alert-success" role="alert">
                            {{ $msg }}
                        </div>
                    @endif
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Заголовок</th>
                            <th>Адрес</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($pages as $page)
                            <tr>
                                <td>{{$page->title}}</td>
                                <td>{{$page->slug_with_slash}}</td>
                                <td><span class="badge badge-secondary">active</span></td>
                                <td><a href="{{route('admin.page.edit', $page->id)}}" class="btn btn-primary">Редактировать</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $pages->links('vendor.pagination.bootstrap-4') }}
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card">
                <div class="card-body">
                    <a href="{{route('admin.page.create')}}" class="btn btn-secondary">Добавить</a>
                </div>
            </div>
        </div>
    </div>
@endsection