@extends('layouts.admin')


@section('content')
    <div class="row">
        <div class="col-md-9">
            <form action="{{route('admin.page.update',$page->id)}}" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
                {{method_field('PUT')}}
                <div class="card card-accent-secondary">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i>
                       Изменить страницу
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="form-group row">
                            <label class="col-md-3 form-control-label" for="text-input">Заголовок</label>
                            <div class="col-md-9">
                                <input type="text" id="text-input" name="title" class="form-control" placeholder="Заголовок" value="{{$page->title}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 form-control-label" for="text-input">Путь</label>
                            <div class="col-md-9">
                                <input type="text" id="text-input" name="slug" class="form-control" placeholder="Путь" value="{{$page->slug}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 form-control-label" for="text-input">Контент</label>
                            <div class="col-md-9">
                                <textarea id="summernote" name="content" >{{$page->content}}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i>Сохранить</button>
                        <button type="reset" class="btn btn-sm btn-danger"><i class="fa fa-ban"></i>Сбросить</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-3">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('admin.page.destroy', $page->id)}}" method="POST">
                        {{method_field('DELETE')}}
                        {{csrf_field()}}
                        <button type="submit" class="btn btn-danger">Удалить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection