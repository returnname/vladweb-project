@extends('layouts.pages-admin')


@section('content')
    <div class="col-md-5">
        <div class="card-group">
            <div class="card p-4">
                @if($errors->any())
                    <div class="uk-alert-danger" uk-alert>
                        <a class="uk-alert-close" uk-close></a>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </div>
                @endif
                <div class="card-body">
                    <form method="POST" action="{{ route('admin.login') }}">
                        {{ csrf_field() }}
                        <h1>Авторизация</h1>
                        <p class="text-muted">Пожалуйста, авторизуйтесь</p>
                        <div class="input-group mb-3">
                            <span class="input-group-addon"><i class="icon-user"></i></span>
                            <input
                                    class="form-control {{ $errors->has('email') ? 'uk-form-danger' : '' }}"
                                    type="text"
                                    name="email"
                                    value="{{ old('email') }}"
                                    placeholder="E-mail"
                            >
                        </div>
                        <div class="input-group mb-4">
                            <span class="input-group-addon"><i class="icon-lock"></i></span>
                            <input class="form-control {{ $errors->has('password') ? 'uk-form-danger' : '' }}"
                                   type="password"
                                   name="password"
                                   placeholder="Пароль"
                            >
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <input type="submit" value="Войти" class="btn btn-primary px-4"/>
                            </div>
                            <div class="col-6 text-right">
                                {{--<a href="{{route('admin.password.request')}}" class="btn btn-link px-0">Forgot password?</a>--}}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection