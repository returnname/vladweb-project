<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link" href="{{route('admin.dashboard')}}">
                    <i class="icon-speedometer"></i> Главная панель </a>
            </li>

            <li class="nav-title">
                Основные
            </li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#"><i class="icon-pencil"></i> Управление</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('admin.coach.index')}}">
                            <i class="icon-user"></i>Тренера</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('admin.mountain.index')}}">
                            <i class="icon-direction"></i>Горные базы</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('admin.review.index')}}">
                            <i class="icon-speech"></i>Отзывы</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a class="nav-link" href=" {{route('admin.page.index')}}">
                    <i class="icon-docs"></i>Cтраницы</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('admin.user.index')}}">
                    <i class="icon-people"></i>Пользователи</a>
            </li>
            {{--<li class="nav-item">--}}
                {{--<a class="nav-link" href=" {{route('admin.page.index')}}">--}}
                    {{--<i class="icon-settings"></i>Настройки</a>--}}
            {{--</li>--}}
        </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>