@switch($status)
    @case('active')
        <span class="badge badge-success">{{$status}}</span>
        @break
    @case('moderating')
        <span class="badge badge-warning">{{$status}}</span>
        @break
    @case('disabled')
        <span class="badge badge-danger">{{$status}}</span>
        @break
@endswitch