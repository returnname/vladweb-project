@if ($paginator->hasPages())
<nav>
    <ul class="pager pager-sm">
            @if ($paginator->onFirstPage())
                <li><a rel="prev"><span class="icon-left mdi mdi-arrow-left" aria-hidden="true"></span>@lang('pagination.previous')</a></li>
            @else
                <li><a href="{{$paginator->previousPageUrl()}}" rel="prev"><span class="icon-left mdi mdi-arrow-left" aria-hidden="true"></span>@lang('pagination.previous')</a></li>
            @endif
            @if ($paginator->hasMorePages())
                    <li><a href="{{ $paginator->nextPageUrl() }}" rel="next">@lang('pagination.next')<span class="icon-right mdi mdi-arrow-right" aria-hidden="true"></span></a></li>
            @else
                    <li><a rel="next">@lang('pagination.next')<span class="icon-right mdi mdi-arrow-right" aria-hidden="true"></span></a></li>
            @endif
    </ul>
</nav>
@endif