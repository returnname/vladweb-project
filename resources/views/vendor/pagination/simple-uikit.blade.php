

@if ($paginator->hasPages())
<ul class="uk-pagination uk-flex-center">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="uk-disabled"><a href="{{ $paginator->previousPageUrl() }}" rel="prev"><span class="uk-margin-small-right" uk-pagination-previous></span>@lang('pagination.previous')</a></li>
        @else
            <li class=><a href="{{ $paginator->previousPageUrl() }}" rel="prev"><span class="uk-margin-small-right"uk-pagination-previous></span>@lang('pagination.previous')</a></li>
        @endif
        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li><a href="{{ $paginator->nextPageUrl() }}" rel="next">@lang('pagination.next')<span class="uk-margin-small-left" uk-pagination-next ></span></a></li>
        @else
            <li class="uk-disabled" ><a href="{{ $paginator->nextPageUrl() }}" rel="next">@lang('pagination.next')<span class="uk-margin-small-left" uk-pagination-next ></span></a></li>
        @endif
</ul>
@endif