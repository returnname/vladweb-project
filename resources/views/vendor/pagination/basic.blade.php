@if ($paginator->hasPages())
<ul class="pagination">
    {{-- Previous Page Link --}}
    @if ($paginator->onFirstPage())
        <li><a class="pagination__link disabled"><i class="fas fa-angle-left"></i></a></li>
    @else
        <li><a class="pagination__link" href="{{ $paginator->previousPageUrl() }}" rel="prev"><i class="fas fa-angle-left"></i></a></li>
    @endif

    @if($elements)
    {{-- Pagination Elements --}}
    @foreach ($elements as $element)
        {{-- "Three Dots" Separator --}}
        @if (is_string($element))
            <li><span class="pagination__link">{{ $element }}</span></li>
        @endif

        {{-- Array Of Links --}}
        @if (is_array($element))
            @foreach ($element as $page => $url)
                @if ($page == $paginator->currentPage())
                    <li ><span class="pagination__link active">{{ $page }}</span></li>
                @else
                    <li><a class="pagination__link" href="{{ $url }}">{{ $page }}</a></li>
                @endif
            @endforeach
        @endif
    @endforeach
    @endif
    {{-- Next Page Link --}}
    @if ($paginator->hasMorePages())
        <li><a class="pagination__link" href="{{ $paginator->nextPageUrl() }}" rel="next"><i class="fas fa-angle-right"></i></a></li>
    @else
        <li><span class="pagination__link disabled"><i class="fas fa-angle-right"></i></span></li>
    @endif
</ul>
@endif
