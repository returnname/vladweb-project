 @if (count($breadcrumbs))
        <ol class="breadcrumb">
            @foreach ($breadcrumbs as $breadcrumb)
                @if ($breadcrumb->url && !$loop->last)
                    <li class="breadcrumb-item"><a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a></li>
                @else
                    <li class="breadcrumb-item active">{{ $breadcrumb->title }}</li>
                @endif
            @endforeach
            <!-- Breadcrumb Menu-->
            {{--<li class="breadcrumb-menu d-md-down-none">--}}
            {{--<div class="btn-group" role="group" aria-label="Button group">--}}
            {{--<a class="btn" href="#"><i class="icon-speech"></i></a>--}}
            {{--<a class="btn" href="./"><i class="icon-graph"></i> &nbsp;Dashboard</a>--}}
            {{--<a class="btn" href="#"><i class="icon-settings"></i> &nbsp;Settings</a>--}}
            {{--</div>--}}
            {{--</li>--}}
        </ol>
 @endif