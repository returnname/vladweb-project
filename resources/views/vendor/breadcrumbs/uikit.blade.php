@if (count($breadcrumbs))
    <div class="uk-container uk-padding-small">
        <ul class="uk-breadcrumb">
            @foreach ($breadcrumbs as $breadcrumb)
                @if ($breadcrumb->url && !$loop->last)
                    <li><a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a></li>
                @else
                    <li class="uk-disabled"><a>{{ $breadcrumb->title }}</a></li>
                @endif
            @endforeach
        </ul>
    </div>
@endif