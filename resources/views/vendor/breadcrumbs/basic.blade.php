
@if (count($breadcrumbs))
    <nav class="breadcrumbs container mx-auto">
        <ol class="breadcrumbs-list">
        @foreach ($breadcrumbs as $breadcrumb)
            @if ($breadcrumb->url && !$loop->last)
                <li><a class="breadcrumbs-list__item" href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a></li>
            @else
                <li class="breadcrumbs-list__item active">{{ $breadcrumb->title }}</li>
            @endif
        @endforeach
        </ol>
    </nav>
@endif