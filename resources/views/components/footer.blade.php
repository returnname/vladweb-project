<!-- footer -->
<footer class="footer">
    <div class="row justify-content-around align-items-start container2">
        <div class="col-md-3 col-sm-6 col-12">
            <img src="{{asset('images/bycycle1.png')}}" alt="bycycle">
            <div class="footer-logo-text">COMPANY LOGO</div>
            <span class="footer-logo-slogan">АГРЕГАТОР ВАШЕГО ОТДЫХА</span>
            <div class="copyright">
                © 2018 Базы отдыха, аренда <br>
                оборудования и экипировки
            </div>
            <div class="social-icons">
                <a href="#"><img src="{{asset('images/facebook-icon.png')}}"></a>
                <a href="#"><img src="{{asset('images/whatsup-icon.png')}}"></a>
                <a href="#"><img src="{{asset('images/viber-icon.png')}}"></a>
                <a href="#"><img src="{{asset('images/vk-icon.png')}}"></a>
                <a href="#"><img src="{{asset('images/ok-icon.png')}}"></a>
                <a href="#"><img src="{{asset('images/instagram-icon.png')}}"></a>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-12">
            <h1>О КОМПАНИИ</h1>
            <ul class="footer-menu">
                <li><a href="#">Офисы продаж</a></li>
                <li><a href="#">О нас</a></li>
                <li><a href="#">Пресса о нас</a></li>
                <li><a href="#">Отзывы о нас</a></li>
                <li><a href="#">Наши партнёры</a></li>
                <li><a href="#">Реквизиты</a></li>
            </ul>
        </div>
        <div class="cooperation col-md-3 col-sm-6 col-12">
            <img src="{{asset('images/sberbank-icon.png')}}">
            <img src="{{asset('images/otkrytie-icon.png')}}">
            <img src="{{asset('images/skolkovo-icon.png')}}">
            <img src="{{asset('images/nz-icon.png')}}">
            <img src="{{asset('images/mir-icon.png')}}">
            <img src="{{asset('images/visa-icon.png')}}">
            <img src="{{asset('images/mastercard-icon.png')}}">
            <img src="{{asset('images/ata-icon.png')}}">
        </div>
        <div class="col-md-3  col-sm-6  col-12 footer-block4">
            <div class="footer-phone-block">
                <span><i class="fas fa-phone-volume"></i>&nbsp;&nbsp;Нужна консультация?</span>
                <div>+7 920 625 28-14</div>
            </div>
            <br>
            <a href="#" class="store-link"><img src="{{asset('images/app-store.png')}}"></a>
            <br>
            <a href="#" class="store-link"><img src="{{asset('images/play-market.png')}}"></a>
        </div>
    </div>
</footer>
<!--/footer -->