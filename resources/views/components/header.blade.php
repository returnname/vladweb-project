<div id="top">
    <div class="row justify-content-center align-items-center container1">
        <div class="col-xl-2 col-md-3 col-sm-4 col-6 top1">
            <i class="fas fa-map-marker-alt gps-marker"></i>
            <div class="top1-sel-box">
                <select class="top1-sel ">
                    <option>Владимир</option>
                    <option>Омск</option>
                    <option>Орёл</option>
                </select>
            </div>
            <i class="fas fa-angle-down" id="sel-arrow"></i>
        </div>

        <div class="col-xl-8 col-md-7 col-sm-4  col-2 col-top-menu">
            <nav class="navbar navbar-expand-xl">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto top-menu">
                        <li class="nav-item active">
                            <a class="nav-link" href="{{route('mountain.index')}}">Базы отдыха</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#">Вело</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Сноуборд и лыжи</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Активный отдых</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Экипировка</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Инструкторы</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">О нас</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Частые вопросы</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <div class="col-xl-2 col-md-2 col-sm-4  col-4">
            @auth
                <button class="btn btn-outline-light authorization dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{ auth()->user()->name }}
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <form id="logout" action="{{route('logout')}}" method="post">
                        {{csrf_field()}}
                    </form>
                    <a class="dropdown-item" href="{{route('profile')}}">Профиль</a>
                    
                    <button type="submit" form="logout" class="dropdown-item" href="#">
                        Выйти
                    </button>
                </div>
            @else
                <a href="{{route('login')}}" class="btn btn-outline-light authorization">
                    <i class="fas fa-user"></i>   Войти
                </a>
            @endauth

        </div>
    </div>
</div>






