import InputMask from 'inputmask';

if (document.getElementById('telephone')){
    let selector = document.getElementById('telephone');
    let im = new InputMask("+7 (999) 999-99-99").mask(selector);

    selector.oninput = (e)=>{console.log(im.isValid())};
}

let domain = document.getElementById('domain');

if (domain){
    domain.onchange = e => {
        axios.post('/checkdomain',{ domain : e.target.value })
            .then(d=>{
                console.log(d.data)
            })
            .catch(error=>{
                console.log(error.response)
            });
    }
}
