import React, {Component} from 'react';

export default class EquipmentCategory extends Component {
    render() {
        const category = this.props.category;

        const CategoryItems =  category.equipments.map((eq)=>{
            return(
                <li className="equipment-item" key={eq.id}>
                    <span className="equipment-item__name">{eq.name + '(+' + eq.price + 'р.)'}</span>
                    <input type="checkbox" onChange={()=>{this.props.onChangeEquipment(eq.id)}}/>
                </li>
            )
        });

        return (
            <div className="equipment-category">
                <div className="equipment-category__title">
                    {category.name}
                </div>

                <ul className="equipment-category__items">
                    {CategoryItems}
                </ul>
            </div>
        )
    }
}
