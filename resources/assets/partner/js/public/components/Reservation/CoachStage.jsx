import React, {Component} from 'react';
import PropTypes from 'prop-types';

import Loading from '../Loading';

import _ from 'lodash';

import CoachList from '../Coach/CoachList'
import CoachTimePicker from '../Coach/CoachTimePicker';



export default class CoachStage extends Component {
    constructor(props){
        super(props);

        this.state = {
            coachId: null,
            needRefresh:false
        }
    }

    render() {
        const { coachId , needRefresh } = this.state;
        const { coaches } = this.props;

        if( this.props.coaches.length == 0 ) return <Loading/>;

        const availableTime = this.state.coachId ? this.availableTime() : [];

        return (
            <div className="coach-chooser flex">
                <div className="w-full mb-6 lg:mb-0 lg:w-1/2 px-4 flex flex-col mt-6">
                    <CoachList
                        coaches={coaches}
                        coachId={coachId}
                        onChangeCoach={this.handleChangeCoach.bind(this)}
                    />
                </div>
                <div className="w-full mb-6 lg:mb-0 lg:w-1/2 px-4 flex flex-col mt-6">
                    {coachId && <CoachTimePicker
                        onChangeTime={this.handleChangeTime.bind(this)}
                        times={availableTime}
                    />}
                </div>
            </div>
        );
    }

    handleChangeCoach(id){
        let coachId = (this.state.coachId === id)? null : id ;


        this.setState({ coachId });
    }

    handleChangeTime(selectedTime){
        const  { coachId }  = this.state;

        if (!coachId) return console.log('Select coach');

        this.props.onChangeCoachInfo({ selectedTime , coachId })
    }

    availableTime(){
        const { start , end } = this.context.workingHours;

        const selectedCoach = this.props.coaches.find(c =>(c.id == this.state.coachId));

        const reservations = selectedCoach.reservations.map(res => {
            return {
                dateStart : moment(res.time_start),
                dateEnd : moment(res.time_end)
            }
        });

        return _.range( start, end ).map(current=>{
            const date = this.context.reserveDate.dateStart.clone().hour(current);

            let disableIndex = reservations.find(res => date.isBetween(res.dateStart, res.dateEnd , null , '[]'))

            return {
                date,
                isDisabled: disableIndex ? true : false
            }
        });
    }
}

CoachStage.propTypes = {
    coaches: PropTypes.array
};

CoachStage.contextTypes = {
    reserveDate: PropTypes.object.isRequired,
    workingHours: PropTypes.object.isRequired
};


