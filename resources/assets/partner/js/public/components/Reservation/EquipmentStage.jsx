import React, {Component} from 'react';
import EquipmentCategory from '../Equipment/EquipmentCategory';
import _ from 'lodash';

export default class EquipmentStage extends Component {
    constructor(props){
        super(props);

        this.categoryRender = this.categoryRender.bind(this);
    }
    render() {
        const equipment = this.props.equipment.categories;

        if (equipment.length == 0){
            return <img src="../../../images/loader.gif" alt=""/>
        }

        let Categories = this.categoryRender();


        return (
            <React.Fragment>
                <div className="equipment">
                    <div className="equipment-categories-container">
                        {Categories[0]}
                    </div>
                    <div className="equipment-categories-container">
                        {Categories[1]}
                    </div>
                </div>
            </React.Fragment>
        );
    }
    categoryRender(){
        let categories = this.props.equipment.categories,
            equipments = this.props.equipment.items,
            chunkLength = 0;

        if (categories.length>3)  chunkLength = _.ceil(categories.length/2);

        let renderedCategories = categories.map((category)=>{
            if (category.equipments.length == 0) return;

            return <EquipmentCategory category={category} key={category.id} onChangeEquipment={this.props.onChangeEquipment}/>
        });


        return _.chunk(renderedCategories,chunkLength);
    }
}


