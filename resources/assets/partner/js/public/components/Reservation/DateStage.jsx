import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Calendar from '../Calendar/CalendarComponent';

import InputRange from 'react-input-range';

export default class DateStage extends Component {
    constructor(props , context){
        super(props);

        const { start } = context.workingHours;

        this.state = {
            value :{
                min: start,
                max: start + 1
            }
        };
    }
    render() {
        const { value } = this.state;
        const { workingHours } = this.context;

        return (
            <div className="time-chooser">
                <div className="flex">
                    <div className="w-1/2 text-center">
                        <h3 className="mb-6 text-2xl p-4">Выберите день</h3>
                        <Calendar {...this.props}/>
                    </div>
                    <div className="w-1/2 text-center">
                        <h3 className="mb-6 text-2xl p-4">Выберите время</h3>
                        <InputRange
                            formatLabel={value => `${value}:00`}
                            maxValue={workingHours.end}
                            minValue={workingHours.start}
                            value={value}
                            onChange={this.handleChangeTime.bind(this)}
                        />
                    </div>
                </div>
            </div>
        );
    }

    handleChangeTime(value){
        this.setState({ value });

        this.props.onChangeTime({ dateStart: { hour: value.min}, dateEnd: { hour: value.max}});
    }
}

DateStage.contextTypes = {
    workingHours: PropTypes.object.isRequired
};



