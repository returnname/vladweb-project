import React, {Component} from 'react';
import PropTypes from 'prop-types';


export default class ReservationInfoStage extends Component {
    constructor(props){
        super(props)
        console.log(props);
    }
    render() {
        const { selectedCoach , selectedTime , selectedEquipment} = this.props.reservationInfo;
        const { price } = this.props;

        return (
            <div className="w-full mb-6 lg:mb-0 px-4 flex flex-col mt-6">
                <div className="flex-grow flex flex-col bg-white border-t border-b sm:rounded sm:border shadow overflow-hidden">
                    <div className="px-6 border-b">
                        <h3 className="text-blue-dark py-4 font-normal text-lg">Информация о заказе</h3>
                    </div>
                    <div className="w-full bg-blue-lightest p-4">
                        <div className="mt-4">
                            <p className="font-bold">Дата: <span className="font-normal">{selectedTime.dateStart.format('YYYY-MM-DD')}</span></p>
                        </div>
                        <div className="mt-4">
                            <p className="font-bold">Время: <span className="font-normal">{selectedTime.dateStart.format('H-mm')} до {selectedTime.dateEnd.format('H-mm')}</span></p>
                        </div>

                        {selectedCoach && <div className="mt-4">
                            <p className="font-bold">Тренер <span className="font-normal">{selectedCoach.coachInfo.full_name}</span></p>
                            <p>Время c {selectedCoach.selectedTime.start.date.format('h-mm')} до {selectedCoach.selectedTime.end.date.format('h-mm')}</p>
                        </div>}

                        {selectedEquipment && selectedEquipment.length!=0 &&
                        <div className="mt-4">
                            <p className="font-bold">Экипировка:</p>
                            <ul className="p-4 ml-4 bg-red-lightest list-reset">
                                {selectedEquipment.map(eq=><li key={eq.id} className="p-4">{eq.name}</li>)}
                            </ul>
                        </div>
                        }


                        <div className="mt-4">
                            <p className="font-bold">Стоимость:
                                <span className="font-normal">
                                    { price } р.
                                </span>
                            </p>
                        </div>

                        <div className="mt-4">
                            <a className="btn btn-blue">
                                Оплатить
                            </a>
                        </div>

                    </div>


                </div>
            </div>
        );
    }

}

ReservationInfoStage.propTypes = {
    reservationInfo : PropTypes.object.isRequired,
};

