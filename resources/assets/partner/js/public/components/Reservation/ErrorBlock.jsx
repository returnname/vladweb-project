import React from 'react'

const ErrorBlock = ({error}) => {
    return(
        <div className="text-white w-full bg-red-light p-4 mt-4 rounded">
            <i className="mr-4 fas fa-exclamation-circle"/>
            {error}
        </div>
    );
};
export default ErrorBlock;

