import React from 'react'

const HeaderStage = ({stage , current}) => {
    let stageClass = "reserve-stages__item";
    if (stage.key == current) stageClass += " active";
    return(
        <div className={stageClass}>
            {stage.text}
        </div>
    );
}
export default HeaderStage;

