import React from 'react';


const CoachTimePickerItemTypeBadge = (props) => {
     let clsName = 'select-type';
     clsName += !props.textRight ? ' text-left' : ' text-right' ;

     return (
         <div className={clsName}>
             {props.text}
         </div>
     )
};


export default CoachTimePickerItemTypeBadge;



