import React, {Component} from 'react';

import PropTypes from "prop-types";

import TimeItem from './CoachTimePickerItem';

export default class CoachTimePicker extends Component {
    constructor(props){
        super(props);

        this.state = {
            times : [],
            hoverIndex: null,
            step:0
        }
    }

    getStep(){
        if (!this.state.startIndex) return 0;
        if (!this.state.endIndex) return 1;

        return 2;
    }

    getChildContext() {
        const { hoverIndex , startIndex , endIndex  , step } = this.state;

        return {
            hoverIndex,
            startIndex,
            endIndex,
            step
        };
    }
    componentWillUpdate(){
        const { needRefresh } = this.props;

        if (needRefresh) this.refresh()
    }

    render() {
        const { hoverIndex , startIndex , endIndex } = this.state;
        const { times , needRefresh } = this.props;

        const TimeList = times.map((hour, timeIndex) =>{

            const isDisabled = hour.isDisabled;
            const isHover = timeIndex == hoverIndex && !isDisabled;
            const inRange = (timeIndex < hoverIndex && timeIndex > startIndex && !isDisabled && startIndex);
            const isSelected = startIndex == timeIndex || endIndex == timeIndex;
            const inSelected = (timeIndex > startIndex && timeIndex < endIndex);


            return <TimeItem
                itemClass="time-item"
                key={timeIndex}
                timeIndex={timeIndex}
                value={hour.date}
                isDisabled={isDisabled}
                isHover={isHover}
                isSelected={isSelected}
                inRange={inRange}
                inSelected={inSelected}
                onSetTime={this.handleSelectTime.bind(this)}
                onMouseEnter={this.handleMouseEnter.bind(this)}
                onMouseLeave={this.handleMouseLeave.bind(this)}
            />
        });

        return (
            <React.Fragment>
                <div className="flex-grow flex flex-col bg-white border-t border-b sm:rounded sm:border shadow overflow-hidden">
                    <div className="px-6 border-b">
                        <h3 className="text-blue-dark py-4 font-normal text-lg">Выбрать время</h3>
                    </div>
                    <div>
                        {TimeList}
                    </div>
                </div>
            </React.Fragment>
        );
    }

    handleSelectTime(value){
        let { step } = this.state;
        const { times } = this.props;

        let selectedIndex = times.findIndex(t => t.date.isSame(value));
        let nextStateIndex;

        if (step === 0){
            nextStateIndex = { startIndex: selectedIndex , step }
        }

        if (step === 1){
            const { startIndex  } = this.state;
            nextStateIndex = { endIndex: selectedIndex };

            let notSelectable = times.slice(startIndex, selectedIndex).find(t => t.isDisabled);
            if (notSelectable || selectedIndex < startIndex) return;

            this.props.onChangeTime({ start: times[startIndex], end: times[selectedIndex]})
        }

        if (step === 2 ){

            return this.refresh()
        }

        step++;

        this.setState({ ...nextStateIndex, step });
    }

    handleMouseEnter(val){
        let hoverIndex = this.props.times.findIndex(t => t.date.isSame(val))

        this.setState({hoverIndex})
    }

    handleMouseLeave(){
        let hoverIndex = null;

        this.setState({hoverIndex})
    }

    refresh(){

        this.setState({ step:0, startIndex:null , endIndex:null});
    }
}


CoachTimePicker.contextTypes = {
    reserveDate: PropTypes.object.isRequired,
    workingHours: PropTypes.object.isRequired
};

CoachTimePicker.propTypes = {
    reservations: PropTypes.array
};

CoachTimePicker.childContextTypes = {
    hoverIndex: PropTypes.number,
    startIndex:  PropTypes.number,
    endIndex : PropTypes.number,
    step : PropTypes.number
};


