import React, {Component} from 'react';
import PropTypes from "prop-types";

import classNames from 'classnames';
import Badge from './CoachTimePickerItemTypeBadge';

class CoachTimePickerItem extends Component {

    render(){
        const { value, itemClass , inRange , isDisabled , isHover , isSelected , inSelected } = this.props;
        const { step } = this.context;

        let nameClass = classNames({
            [itemClass]:true,
            'in-range': inRange,
            'on-hover': isHover,
            'selected': isSelected,
            'disabled': isDisabled,
            'in-selected': inSelected,
        });

        let badgeText =  step == 2  ? '' : this.getBadgeText();

        return(
            <li
                className={nameClass}
                onMouseLeave={()=>{this.props.onMouseLeave()}}
                onClick={()=>{this.handleClick()}}
                onMouseEnter={()=>{this.props.onMouseEnter(value)}}
            >
                { step!=1 && <Badge text={badgeText}  />}
                { step==1 && <Badge text={badgeText} textRight /> }
                <a>
                    {value.format('H:mm')}
                </a>
            </li>
        );
    }
    handleClick(){
        const { value , onSetTime, isDisabled} = this.props;


        onSetTime(value);
    }

    getBadgeText(){
        const {step} = this.context;

        if (this.props.isDisabled) return 'занято';

        switch (step){
            case 0:
                return 'начало';
            case 1:
                return 'конец';
            default:
                return 'asd'
        }
    }




}export default CoachTimePickerItem;


CoachTimePickerItem.contextTypes = {
    hoverIndex: PropTypes.number,
    startIndex:  PropTypes.number,
    endIndex :PropTypes.number,
    step :PropTypes.number
};