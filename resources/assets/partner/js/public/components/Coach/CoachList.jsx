import React, {Component} from 'react';
import PropTypes from "prop-types";

import Rating from 'react-rating';

export default class CoachList extends Component {
    render() {
        const { coachId , coaches} = this.props;

        if (!coaches) return <div> Доступных тренеров нету</div>

        const CoachList = coaches.map(coach=>{
            let coachClass = 'coach-item';

            if (coachId === coach.id) coachClass += ' active';

            return(
                <div className={coachClass} key={coach.id} onClick={()=>{this.props.onChangeCoach(coach.id)}}>
                    <div className="w-2/5 xl:w-1/4 px-4 flex items-center">
                        <span className="text-lg">{coach.firstname + ' ' +coach.lastname}</span>
                    </div>
                    <div className="hidden md:flex lg:hidden xl:flex w-1/4 px-4 items-center">
                        Сноубород
                    </div>
                    <div className="flex w-3/5 md:w/12">
                        <div className="w-1/2 px-4">
                            <div className="text-right text-orange-lightest">
                                <Rating
                                    initialRating={coach.rating}
                                    emptySymbol="far fa-star"
                                    fullSymbol="fas fa-star"
                                    readonly
                                />
                            </div>
                        </div>
                        <div className="w-1/2 px-4">
                            <div className="text-right text-grey">
                                {coach.price} р.
                            </div>
                        </div>
                    </div>
                </div>
            )
        });

        return (
            <div className="flex-grow flex flex-col bg-white border-t border-b sm:rounded sm:border shadow overflow-hidden">
                <div className="border-b">
                    <div className="flex justify-between px-6 -mb-px">
                        <h3 className="text-blue-dark py-4 font-normal text-lg">Доступные тренера</h3>
                    </div>
                </div>
                <div className="border-b">
                    <div className="flex justify-between px-6 py-2">
                        <span className="text-xs">Имя</span>
                        <span className="text-xs">Специализация</span>
                        <span className="text-xs">Рейтинг</span>
                        <span className="text-xs">Стоимость</span>
                    </div>
                </div>
                {CoachList}
            </div>
        );
    }
}

CoachList.propTypes = {
    coaches: PropTypes.array,
    coachId: PropTypes.number,
};


