import React from 'react'

export default () => {
    return(
        <div className='w-full flex justify-center'>
            <span className="p-8"><img src="../../images/loader.gif" alt=""/></span>
        </div>
    );
}


