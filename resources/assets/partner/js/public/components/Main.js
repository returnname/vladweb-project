import React, { Component } from 'react';
import PropTypes from 'prop-types';


import DateStage from './Reservation/DateStage';
import EqipmentStage from './Reservation/EquipmentStage';
import CoachStage from './Reservation/CoachStage';


import ReserveStageHeader from './Reservation/ReserveStageHeader';
import ErrorBlock from './Reservation/ErrorBlock';
import ReservationInfoStage from "./Reservation/ReservationInfoStage";

export default class Reservation extends Component {
    constructor(props) {
        super(props);

        this.state = {
            stage: 1,
            error: null,
            container:{
                selectedCoach: null,
                selectedTime: null,
                selectedEquipment: null
            }
        };

        this.handleChangeTime = this.handleChangeTime.bind(this);
        this.handleChangeEquipment  = this.handleChangeEquipment.bind(this);
    }

    getChildContext() {
        const { workingHours } = this.props;
        const { selectedTime } = this.state.container;

        return {
            reserveDate: selectedTime,
            workingHours
        };
    }

    componentDidMount(){
        axios.get('/api/reserve').then((res)=>{
            this.setState({
                equipment:{
                    categories : res.data.categories,
                    items : res.data.equipments,
                },
                coaches: res.data.coaches
            });
        })
    }

    render() {
        const { stages } = this.props;
        const { error , stage } = this.state;

        const Header = stages.map((stage,key)=>{
            return <ReserveStageHeader stage={stage} key={key} current={this.state.stage} />
        });

        return (
            <React.Fragment>
                <h1 className="p-4">Бронирование</h1>
                <div className="reserve-stages">
                    {Header}
                </div>

                { error && <ErrorBlock  error={error}/> }

                <div className="reserve-price p-4 w-full bg-blue-lightest mt-6 rounded clearfix">
                    <div className="float-right">
                        Цена:<span className="text-xl font-medium ml-2">{this.getFullPrice()}</span>
                    </div>
                </div>

                {this.dispatchStage()}

                <div className="flex justify-around py-8 ">
                    {stage!=1 && <div className="btn btn-blue" onClick={()=>{this.handleChangeStage(-1)}}>Назад</div>}
                    {stage!=4 && <div className="btn btn-blue" onClick={()=>{this.handleChangeStage(1)}}>Вперед</div>}
                </div>
            </React.Fragment>
        );
    }

    dispatchStage(){
        const { stage } = this.state;

        switch (stage){
            case 1:
                return (
                    <DateStage
                        onChangeTime={this.handleChangeTime}
                    />
                );
            case 2:
                return <EqipmentStage equipment={this.state.equipment} onChangeEquipment={this.handleChangeEquipment}/>;
            case 3:
                return(
                    <CoachStage
                        coaches={this.state.coaches}
                        onChangeCoachInfo={this.handleChangeCoach.bind(this)}
                    />
                );
            case 4:
                return(
                    <ReservationInfoStage
                        reservationInfo={this.state.container}
                        price={this.getFullPrice()}
                    />
                );
            default:
                return 'база';
        }
    }

    handleChangeTime(dt){
        const { selectedTime } = this.state.container;

        let dateStart,
            dateEnd;

        if (!selectedTime){
            dateStart = moment().set(dt.dateStart).startOf('hour');
            dateEnd = dateStart.clone().set(dt.dateEnd);
        }else {
            dateStart = selectedTime.dateStart.clone().set(dt.dateStart);
            dateEnd = selectedTime.dateEnd.clone().set(dt.dateEnd);
        }

        this.setState(prevState => {
            return{
                ...prevState,
                container:{ ...prevState.container, selectedTime: { dateStart , dateEnd } }
            }
        });
    }

    handleChangeStage(stg){
        const { stage } = this.state;
        const nextStage = stage + stg;

        const error = this.validate();

        if (error) return this.setState({ error });

        if ( nextStage < 0 || nextStage > 4 ) return;

        this.setState({
            stage: nextStage,
            error: null
        });
    }

    handleChangeEquipment(id){
        let { selectedEquipment } = this.state.container;

        const equipmentItem  = this.state.equipment.items.find( eq => eq.id == id );


        if (!selectedEquipment || (selectedEquipment.length == 0)){
            selectedEquipment = [ equipmentItem ];
        }else {
            if (selectedEquipment.find( eq => eq.id == id )){
                selectedEquipment = selectedEquipment.filter(eq => eq.id != id);
            }else {
                selectedEquipment.push(equipmentItem);
            }
        }

        this.setState(prevState => {
            return{
                ...prevState,
                container:{ ...prevState.container, selectedEquipment }
            }
        });
    }

    handleChangeCoach(selectedCoach){
        const coachInfo = this.state.coaches.find( c => c.id == selectedCoach.coachId);

        selectedCoach = {
            ...selectedCoach,
            coachInfo
        };

        this.setState(prevState => {
            return{
                ...prevState,
                container:{ ...prevState.container, selectedCoach}
            }
        });
    }

    validate(){
        const { container } = this.state;
        const { stages } = this.props;

        let stage = stages.find(stage=>(this.state.stage == stage.key));

        if(!stage.validateStage) return;

        return stage.validation( container, 'Выберите время');
    }


    getFullPrice(){
        const { selectedCoach , selectedTime , selectedEquipment} = this.state.container;

        let fullCost = 0;

        if (selectedTime){
            const { dateStart , dateEnd } = selectedTime;

            let timeLength = dateEnd.diff( dateStart, 'hours');

            fullCost += timeLength * 200;
        }

        if (selectedCoach){
            const { start , end } = selectedCoach.selectedTime;
            let timeLength = end.date.diff(start.date , 'hours');

            fullCost += timeLength * selectedCoach.coachInfo.price;
        }

        if (selectedEquipment){
            selectedEquipment.map(c => fullCost += c.price);
        }


        return `${fullCost} р.`;
    }

}

Reservation.propTypes = {
    stages: PropTypes.array.isRequired,
    workingHours : PropTypes.object.isRequired,
};

Reservation.childContextTypes = {
    reserveDate: PropTypes.object,
    workingHours: PropTypes.object
};

