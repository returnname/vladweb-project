import React, {Component} from 'react';

import Rating from 'react-rating';



export default class ReviewSelectRating extends Component {
    constructor(props){
        super(props)

        this.state = {
            value: null
        }
    }

    render() {
        const { value } = this.state;

        return (
            <div>
                <Rating
                    emptySymbol="far fa-star"
                    fullSymbol="fas fa-star"
                    onChange={this.handleRatingChange.bind(this)}
                    initialRating={value}
                />
                <input type="hidden" name="rating" id="select-rating-input"/>
            </div>
        );
    }
    handleRatingChange(value){
        document.getElementById('select-rating-input').value = value;

        this.setState({value});
    }
}


