import React from 'react'

export default (props) => {
    let clsName = "calendar-day";
    if (props.disabled) clsName += " disabled";
    if (props.active) clsName += " active";
    return(
        <div className={clsName} onClick={()=>{if (props.disabled) return; props.onSetDay(props.day)}}>
            {props.day}
        </div>
    );
}


