import React, {Component} from 'react';
import _ from 'lodash';

import Day from './CalendarDay';

export default class Calendar extends Component {
    constructor(props){
        super(props);

        this.state = {
            date : moment().locale('ru')
        };

        this.setDay = this.setDay.bind(this);
        this.changeMonth = this.changeMonth.bind(this);
    }

    render() {
        const Calendar = this.renderCalendar();

        return (
            <React.Fragment>
                <div className="calendar mt-6">
                    <div className="calendar__top flex justify-around p-4">
                        <button onClick={()=>{this.changeMonth(-1)}}><i className="fas fa-angle-left"/></button>
                        <span className="font-bold">{this.state.date.format('MMMM')}</span>
                        <button onClick={()=>{this.changeMonth(1)}}><i className="fas fa-angle-right"/></button>
                    </div>
                    <div className="calendar-content">
                        { Calendar}
                    </div>
                </div>
            </React.Fragment>
        );
    }
    changeMonth(val){
        this.setState({
            date: this.state.date.add(val, 'months')
        });
    }
    setDay(date){
        let updatedDate = this.state.date.set('date', date);

        this.props.onChangeTime({ dateStart: { date }, dateEnd: { date } });

        this.setState({
            date: updatedDate
        });
    }
    calendarDates(){
        let monthRange = this.state.date.daysInMonth();
        let startIndex = this.state.date.clone().startOf('month').day() ;

        let calendar = new Array(35);

        (startIndex == 0) ? startIndex = 6 : startIndex--;

        if(startIndex == 6 && monthRange>29){
            calendar = new Array(42);
        }

        for(let i = 1 ; i <= monthRange; i++, startIndex++){
            calendar[startIndex] = i;
        }

        return _.chunk(calendar,7);
    }

    renderCalendar(){
        return this.calendarDates().map((week,key)=>{
            return (
                <div className="calendar-row flex" key={key}>
                    {week.map((day,key)=>{
                        if (!day){ return <Day day={day} key={key} onSetDay={this.setDay} disabled={true}/> }

                        let date = this.state.date.clone().date(day);

                        if (date.unix() === this.state.date.unix()){
                            return <Day day={day} key={key} onSetDay={this.setDay} active={true}/>;
                        }

                        return(
                            <Day day={day} key={key} onSetDay={this.setDay}/>
                        );
                    })}
                </div>
            );
        });
    }
}




