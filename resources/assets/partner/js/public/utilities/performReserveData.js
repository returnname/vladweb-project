export default function (data) {
    let dates = [
        data.dateStart.format('YYYY-MM-D HH:mm:ss'),
        data.dateEnd.format('YYYY-MM-D HH:mm:ss')
    ];

    return{
        dates,
        quantity: +data.time.quantity,
        equipment: data.equip.items ,
        coach_id: data.coach.id,
        amount: data.cost,
    }
}



