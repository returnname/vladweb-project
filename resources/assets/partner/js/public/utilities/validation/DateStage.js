export default (state , error) => {
    const { selectedTime } = state;

    if (!selectedTime || selectedTime.dateStart.isSame(selectedTime .dateEnd)) return error;

    return false;
}