import ReactDOM from 'react-dom';
import React from 'react';


import Reservation from './components/Main';
import CreateRating from './components/ReviewSelectRating';

import '../../../bootstrap';
import DateValidation from "./utilities/validation/DateStage";

const workingHours = { start : 9 , end : 22 };

const stages = [
    {
        text:'Выбираем время',
        key:1,
        validateStage: true,
        validation : DateValidation
    },
    {
        text:'Выбираем экипировку',
        key:2,
        validate: false
    },
    {
        text:'Выбираем тренера',
        key:3,
        validate: false
    },
    {
        text:'Оплата',
        key:4,
        validate: false
    },
];

if (document.getElementById('root')) {
    ReactDOM.render(<Reservation workingHours={workingHours} stages={stages}/>, document.getElementById('root'));
}


if(document.getElementById('select-rating')){
    ReactDOM.render(<CreateRating/>, document.getElementById('select-rating'));
}
